Name:		jwall-tools
Version:	%_version
Release:	%_revision
Summary:	The jwall-tools are a collection of command to ease the management of ModSecurity
Group:		admin
License:	AGPL
URL:		http://www.jwall.org/jwall-tools
Source0:	http://www.jwall.org/download/jwall-tools/jwall-tools-latest-src.zip
BuildRoot:	/home/chris/Projekte/jwall-tools/dist


Group: Applications/System
requires:	jre >= 1.6.0


%description
Brief description of software package.

%prep

%build

%install

%clean

%files
%defattr(-,root,root)
%doc

/usr/bin/jwall
/usr/lib/jwall-tools.jar

%changelog
