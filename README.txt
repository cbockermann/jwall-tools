JWall Tools
===========

The JWall Tools is a set of simple Java-based programs which are helpful for
inspecting and managing ModSecurity configurations.
The tools are bundled in a self-executable Java Archive (jar) and come along
with a simple wrapper-script called 'jwall'.

If you installed the JWall-Tools from rpm or any other package (or by simply
fetching the jwall script and running it without arguments), the tools are
set up inside
		/opt/modsecurity/bin/jwall
and
	        /opt/modsecurity/lib/jwall-tools.jar

Once you've included /opt/modsecurity/bin into your PATH environment, you will
be able to use the tools just by calling 'jwall' on the shell prompt:

	#  export PATH=/opt/modsecurity/bin:$PATH
        #  jwall -v

Invoking the jwall-command without any parameters will provide you with a list
of supported sub-commands.


Further documentation is available at

	https://secure.jwall.org/jwall-tools
