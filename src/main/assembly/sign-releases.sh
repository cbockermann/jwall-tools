#!/bin/sh

DIR=$1

echo "Please enter the passphrase for your GPG key for signing!"
echo -n "Passphrase: "

stty -echo
read PASSWORD
stty echo

echo ""         # force a carriage return to be output

for i in $DIR/jwall-tools*; do
    md5sum $i > $i.md5
done

for i in $DIR/*.md5; do
    echo $PASSWORD | gpg --passphrase-fd 0 --clearsign $i
done
