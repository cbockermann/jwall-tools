<?xml version="1.0" encoding="iso-8859-1" ?>

<xsl:stylesheet xmlns:xsl="http://www.w3.rg/1999/XLS/Transform" version="1.0">


<xsl:template match="DocumentRoot">
<div class="LineDirective">
   DocumentRoot <xsl:select value-of="text()" />
</div>
</xsl:template>


<xsl:template match="VirtualHost">
<div class="VirtualHost">
  <h4> <xsl:select value-of="ServerName/text()" /> </h4>
  <div class="Nested">
      <xsl:apply-templates match="DocumentRoot|AccessLog|ErrorLog" />
  </div>
</div>
</xsl:template>

</xsl:stylesheet>