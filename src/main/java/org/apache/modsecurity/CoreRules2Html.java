/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.apache.modsecurity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;

import org.jwall.tools.CommandLineTool;


/**
 * 
 * This class implements a small tool for creating a HTML visualization of the
 * core-rules rules set. The core-rules are a set of rule provided by Breach Security
 * and distributed with the ModSecurity module.
 * <p/>
 * Running the tool is simply done by issuing
 * <pre>
 *      java org.jwall.tools.CoreRules2Html output-directory core-rules-version.tar.gz
 * </pre>
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * @version 0.1
 *
 */
public class CoreRules2Html
    implements CommandLineTool
{
    public final static String VERSION = "0.1"; 
    
    public final static String NAME = "crs2html";
    
    
    private static String[] RESOURCES = {
        "icons/16x16/stock_test-mode.png",
        "icons/16x16/stock_id.png",
        "icons/16x16/stock_draw-line-with-circle-arrow.png",
        "icons/16x16/stock_notes.png",
        "icons/16x16/stock_insert_endnote.png"
    };

    public static void main( String[] args ){
        CoreRules2Html crs = new CoreRules2Html();
        crs.run(args);
    }
    
    
    /**
     * @see org.jwall.tools.CommandLineTool#getName()
     */
    public String getName(){
        return NAME;
    }

    
    /**
     * @see org.jwall.tools.CommandLineTool#run(java.lang.String[])
     */
    public void run( String args[] ){
        try {

            if( args.length != 2 ){
                System.out.println("\nUsage is: \n\t java -jar CoreRules2Html.jar  output-directory  core-rules-version.tar.gz\n");
                System.out.println("Instead of specifying the  core-rules  file, you can also");
                System.out.println("specify an URL of the rules, which are then automatically");
                System.out.println("fetched from that URL.");
                System.exit(0);
            }


            String baseDir = args[0];
            String source = args[1];
            File bd = new File( baseDir );

            if( ! bd.exists() || ! bd.canWrite() ){
                if( ! bd.mkdir() ){
                    System.out.println("Could not create output directory " + bd.getAbsolutePath() );
                    System.exit(-1);
                }
            }

            InputStream in = null;

            if( args[0].startsWith("http:") ){

                URL url = new URL( source );
                in = url.openStream();

            } else {

                in = new FileInputStream( new File( source ) );

            }

            CoreRulesImport cri = new CoreRulesImport( in );
            cri.rules.setRevision( "1.6.1" );

            String xml = cri.getXMLString();
            
            File xmlOut = new File( baseDir + "/core-rules.xml" );
            System.out.println("Writing XML to " + xmlOut.getAbsolutePath() );
            FileWriter fw = new FileWriter( xmlOut );
            fw.write( xml.replaceAll( "org.jwall.web.filter.ms.CoreRules", "core-rules" ).replaceAll( "org.jwall.web.filter.ms.SecRule", "rule") );
            fw.flush();
            fw.close();
            
            InputStream xslt = CoreRulesImport.class.getResourceAsStream( "/rules-html.xslt" );
            String html = cri.transform( new StringReader( xml ), xslt );

            for( String res : RESOURCES ){
                //System.out.println( "Looking for "+ res );
                File f = new File( res );
                File dest = new File( baseDir + "/" + f.getName() ); 
                InputStream resIn = CoreRules2Html.class.getResourceAsStream( "/" + res ); 
                if( resIn != null ){
                    System.out.println("Extracting "+ res + " to " + dest.getName() );
                    FileOutputStream resOut = new FileOutputStream( dest );
                    int data = resIn.read();
                    while( data >= 0 ){
                        resOut.write( data );
                        data = resIn.read();
                    }

                    resIn.close();
                    resOut.close();
                } else {
                    System.out.println("Resource " + res + " not found!");
                }
            }

            FileWriter w = new FileWriter( new File( baseDir + "/core-rules.html") );
            w.write( html );
            w.flush();
            w.close();

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
