/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.eval.transform;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventType;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.ModSecurityAuditEvent;

/**
 * 
 * This transformation simply removes a header field. The header field to be
 * removed can either be specified by a concrete name of a regular expression in
 * which case it removes all header-field lines matching that expression.
 * 
 * @author chris@jwall.org
 * 
 */
public class RemoveHeaderField implements Transformation {
	String regexp;

	/**
	 * This creates an instance of this class that removes all header-field
	 * lines whose header-field <b>name</b> matches the given regular
	 * expression.
	 * 
	 * @param exp
	 */
	public RemoveHeaderField(String exp) throws PatternSyntaxException {
		Pattern.compile(exp);
		regexp = "^" + exp + ":.*";
	}

	/**
	 * @see
	 * org.jwall.core.eval.transform.Transformation#transform(org.modsecurity
	 * .audit.AuditEvent)
	 */
	public AuditEvent transform(AuditEvent evt) throws TransformationException {
		String[] data = evt.getRawData();

		StringBuffer transformed = new StringBuffer("");

		try {
			String line = "";
			BufferedReader r = new BufferedReader(new StringReader(
					data[ModSecurity.SECTION_REQUEST_HEADER]));
			while (r.ready() && line != null) {
				line = r.readLine();
				if (line != null) {
					if (!line.matches(regexp))
						transformed.append(line + "\n");
				}
			}

			data[ModSecurity.SECTION_REQUEST_HEADER] = transformed.toString();
			return new ModSecurityAuditEvent(data, AuditEventType.ModSecurity2);
		} catch (Exception e) {
			throw new TransformationException(e.getMessage());
		}
	}
}
