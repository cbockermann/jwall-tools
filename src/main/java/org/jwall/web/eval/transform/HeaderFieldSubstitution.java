/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.eval.transform;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventType;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.ModSecurityAuditEvent;

/**
 * 
 * This transformation substitutes a set of strings as they occur in a specific
 * header-field of a request/AuditEvent by a replacement-value.
 * 
 * @author chris@jwall.org
 * 
 */
public class HeaderFieldSubstitution implements Transformation {
	String field = "", oldVal = "", newVal = "";
	Map<String, String> map;

	/**
	 * 
	 * This is just internally used.
	 * 
	 * @param headerField
	 */
	private HeaderFieldSubstitution(String headerField) {
		field = headerField;
		map = new HashMap<String, String>();
	}

	/**
	 * This constructor creates a simple substitution for the given
	 * header-field. This will replace all occurences of <code>oldValue</code>
	 * by <code>newValue</code> in the header-field specified by
	 * <code>headerField</code>.
	 * 
	 * @param headerField
	 *            The field where substitution should take place.
	 * @param oldValue
	 *            The old value to be replaced.
	 * @param newValue
	 *            The new value.
	 */
	public HeaderFieldSubstitution(String headerField, String oldValue,
			String newValue) {
		this(headerField);

		map.put(oldValue, newValue);
	}

	/**
	 * This constructor creates a substition for the given header-field. The
	 * field is checked if it contains any of the keywords of the map, which are
	 * then replaced by their associated value given by the substitution-map.
	 * 
	 * @param headerField
	 *            The header-field that this transformation works on.
	 * @param substitutions
	 *            The (old,new)-pairs that are exchanged, if they occur in the
	 *            header.
	 */
	public HeaderFieldSubstitution(String headerField,
			Map<String, String> substitutions) {
		this(headerField);
		map = substitutions;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jwall.core.eval.Transformation#transform(org.modsecurity.audit.AuditEvent
	 * )
	 */
	public AuditEvent transform(AuditEvent evt) throws TransformationException {
		String[] data = evt.getRawData();

		StringBuffer transformed = new StringBuffer("");

		try {
			String line = "";
			BufferedReader r = new BufferedReader(new StringReader(
					data[ModSecurity.SECTION_REQUEST_HEADER]));
			while (r.ready() && line != null) {
				line = r.readLine();
				if (line != null) {

					if (line.startsWith(field + ": ")) {
						String tl = line.replace(oldVal, newVal);

						for (String k : map.keySet())
							tl = tl.replace(k, map.get(k));

						transformed.append(tl + "\n");
					} else
						transformed.append(line + "\n");
				}
			}

			data[ModSecurity.SECTION_REQUEST_HEADER] = transformed.toString();
			return new ModSecurityAuditEvent(data, AuditEventType.ModSecurity2);
		} catch (Exception e) {
			throw new TransformationException(e.getMessage());
		}
	}
}
