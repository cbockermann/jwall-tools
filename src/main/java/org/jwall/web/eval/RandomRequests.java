/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.eval;

import java.net.URL;
import java.util.Random;

import stream.util.URLUtilities;

public class RandomRequests {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		Random rnd = new Random( 1000L );

		for( int i = 0; i < 1; i++ ){
			String location = "http://modsecurity.fritz.box/";

			int len = rnd.nextInt( 1024 );
			if( len > 0 ){

				StringBuffer s = new StringBuffer();
				while( s.length() < len ){
					s.append( rnd.nextInt( 10 ) );
				}

				location += "?variable=" + s.toString();
			}

			try {
				System.out.println( "Sending request " + i + "  to " + location );
				URL url = new URL( location );
				URLUtilities.readContent(url);
			} catch (Exception e) {
				System.err.println( "Error: " + e.getMessage() );
				//e.printStackTrace();
			}
		}
	}
}
