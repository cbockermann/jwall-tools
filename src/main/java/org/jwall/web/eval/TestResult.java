/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.eval;

import java.util.LinkedList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * <p>
 * This class defines all messages/errors/outcomes from the evaluation of a single
 * test-case, i.e. there should be a one-to-one relationship between test-cases and
 * test-results.
 * </p>
 * <p>
 * A test result instance in turn contains a aggregated status over all evaluations
 * (to be seen as a summary) and several detail messages for each of the expectations.  
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
@XStreamAlias("TestResult")
public class TestResult
{
    public final static String TEST_OK = "OK";
    public final static String TEST_FAILED = "FAILED";
    public final static String TEST_ERROR = "ERROR";
    public final static String TEST_INCOMPLETE = "INCOMPLETE";

    
    @XStreamAsAttribute
    String id;
    
    @XStreamAsAttribute
    String status = TEST_INCOMPLETE;
    
    @XStreamImplicit
    LinkedList<Message> messages = new LinkedList<Message>();
    
    
    
    public TestResult( String id ){
        this.id = id;
        messages = new LinkedList<Message>();
    }


    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }


    /**
     * @param id the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    
    /**
     * 
     * @return
     */
    public List<Message> getMessages(){
        return messages;
    }


    /**
     * @param messages the messages to set
     */
    public void setMessages(LinkedList<Message> messages)
    {
        this.messages = messages;
    }


    /**
     * @return the status
     */
    public String getStatus()
    {
        checkStatus();
        return status;
    }


    /**
     * @param status the status to set
     */
    public void setStatus(String status)
    {
        this.status = status;
    }
    
    
    public void add( Message msg ){
        this.messages.add( msg );
    }
    
    public void checkStatus(){
        
        int min = Message.STATUS_OK;
        
        for( Message msg : messages )
            if( msg.status < min )
                min = msg.status;
        
        if( min <= Message.STATUS_ERROR )
            status = TestResult.TEST_ERROR;
        
        if( min <= Message.STATUS_FAILED )
            status = TestResult.TEST_FAILED;
    }
}
