/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.eval;

import java.io.Serializable;

import org.jwall.web.audit.util.MD5;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;



/**
 * <p>
 * This class represents the simplest case for testing, which basically refers to a single
 * HTTP request. The test case contains the HTTP request data of the client and a few expected
 * values for the server response.
 * </p>
 * <p>
 * The class is intended to be used for single-request tests, which can be fired independently.
 * For more complex testing scenarios, session-based test-cases need to be created.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
@XStreamAlias("TestCase")
public class HttpTestCase
    extends AbstractTestCase
    implements Serializable
{
    /** The unique class ID */
    private static final long serialVersionUID = 8575144705924450758L;

    @XStreamAlias("Target")
    String target;
    
    @XStreamAlias("RequestHeader")
    String requestHeader;
    
    @XStreamAlias("RequestBody")
    String requestBody;
    
    @XStreamAsAttribute
    String id;

    
    
    public HttpTestCase(){
        id = null;
        requestHeader = null;
        requestBody = null;
        target = null;
    }
    
    
    /**
     * 
     * @param dst
     * @param header
     * @param body
     */
    public HttpTestCase( String dst, String header, String body ){
        target = dst;
        requestHeader = header;
        requestBody = body;
        if( body != null && body.length() == 0 )
            requestBody = null;

        id = MD5.md5( target + requestHeader + requestBody );
    }
    
    
    
    public String getId(){
        return id;
    }

    
    /**
     * @return the target
     */
    public String getTarget()
    {
        return target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(String target)
    {
        this.target = target;
    }

    /**
     * @return the requestHeader
     */
    public String getRequestHeader()
    {
        return requestHeader;
    }

    /**
     * @param requestHeader the requestHeader to set
     */
    public void setRequestHeader(String requestHeader)
    {
        this.requestHeader = requestHeader;
    }

    /**
     * @return the requestBody
     */
    public String getRequestBody()
    {
        return requestBody;
    }

    /**
     * @param requestBody the requestBody to set
     */
    public void setRequestBody(String requestBody)
    {
        this.requestBody = requestBody;
    }
}
