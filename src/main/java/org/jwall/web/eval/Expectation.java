/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.eval;

import com.thoughtworks.xstream.annotations.XStreamAlias;


/**
 * This class defines an aspect of an outcome of a test-case evaluation. Basically, the result of a
 * test-case is a server response, which may be described in terms of certain variables referring to
 * parts of the response and their expected values.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
@XStreamAlias("Expect")
public class Expectation
{
    @XStreamAlias("Variable")
    String variable;
    
    @XStreamAlias("Value")
    String value;
    
    public Expectation( String var, String val ){
        variable = var;
        value = val;
    }

    /**
     * @return the variable
     */
    public String getVariable()
    {
        return variable;
    }

    /**
     * @param variable the variable to set
     */
    public void setVariable(String variable)
    {
        this.variable = variable;
    }

    /**
     * @return the value
     */
    public String getValue()
    {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value)
    {
        this.value = value;
    }
}
