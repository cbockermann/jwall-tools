/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.eval;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;

public class RuleTimer extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(RuleTimer.class);
	String key = "message";

	String file;
	String location;
	String ruleId;
	Double tformTime;
	Double opTime;

	Pattern invokePattern = Pattern.compile("Invoking Rule ([a-f0-9]*);");
	Pattern tfTimePattern = Pattern
			.compile("Transformation completed in (\\d+) usec.");
	Pattern opTimePattern = Pattern
			.compile("Operator completed in (\\d+) usec.");

	@Override
	public Data process(Data input) {

		Serializable line = input.get(key);
		if (line != null) {
			log.trace("Processing line {}", line);
			String msg = line.toString();
			int ms = msg.indexOf("Invoking rule ");
			if (ms >= 0) {
				int end = msg.indexOf(";");
				ruleId = msg.substring(ms + "Invoking rule ".length(), end);
				log.debug("Found rule-invocation: {}", msg);
				Matcher lm = invokePattern.matcher(msg);
				if (lm.find()) {
					ruleId = lm.group(1);
					log.debug("Found rule-id: {}", lm.group(1));
				}

				int idx = msg.indexOf("[file ");
				if (idx >= 0) {
					end = msg.indexOf("\"", idx + 7);
					location = "file:" + msg.substring(idx + 7, end);
					file = location;
				}

				idx = msg.indexOf("[line ");
				if (idx >= 0) {
					end = msg.indexOf("\"", idx + 7);
					location += ":" + msg.substring(idx + 7, end);
				}

				idx = msg.indexOf("[id ");
				if (idx >= 0) {
					end = msg.indexOf("\"", idx + 5);
					ruleId = msg.substring(idx + 5, end);
				}

				log.debug("   rule id: {}", ruleId);
				log.debug("   location: {}", location);
			}

			Matcher m = tfTimePattern.matcher(msg);
			if (m.find()) {

				for (int i = 0; i <= m.groupCount(); i++) {
					log.debug("group({}) = {}", i, m.group(i));
				}

				tformTime = new Double(m.group(1));
			}

			m = opTimePattern.matcher(msg);
			if (m.find()) {
				opTime = new Double(m.group(1));
			}

			if (msg.indexOf("Rule returned") >= 0 && tformTime != null
					&& opTime != null) {
				log.debug("end-of-rule line: {}", msg);
				input.put("rule:id", ruleId);
				input.put("rule:file", file);
				input.put("rule:location", location);
				input.put("rule:transformationTime", tformTime);
				input.put("rule:operatorTime", opTime);
				input.put("rule:time", tformTime + opTime);

				opTime = null;
				tformTime = null;

				return input;

			} else {
				return null;
			}
		}

		return null;
	}
}
