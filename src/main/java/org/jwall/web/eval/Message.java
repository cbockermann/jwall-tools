/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.eval;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("Message")
public class Message
{
    public final static int STATUS_OK = 1;
    public final static int STATUS_INFO = 0;
    public final static int STATUS_ERROR = -1;
    public final static int STATUS_FAILED = -100;
    
    @XStreamAsAttribute
    String variable = null;

    @XStreamAsAttribute
    Integer status = 0;

    @XStreamAlias("Text")
    String text;
    
    
    public Message( String text ){
        this( STATUS_INFO, text );
    }
    
    public Message( int status, String txt ){
        this.status = status;
        this.text = txt;
    }
    
    
    public Message( String var, String txt ){
        this.variable = var;
        this.text = txt;
    }

    /**
     * @return the variable
     */
    public String getVariable()
    {
        return variable;
    }

    /**
     * @param variable the variable to set
     */
    public void setVariable(String variable)
    {
        this.variable = variable;
    }

    /**
     * @return the text
     */
    public String getText()
    {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text)
    {
        this.text = text;
    }
}
