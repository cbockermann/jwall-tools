/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.eval;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.http.HttpResponse;

public class XMLResultLogger
    implements ResultLogger
{
    PrintStream out;
    private static XMLResultLogger GLOBAL_LOGGER = null;
    private String evalID = null;
    
    private XMLResultLogger( String id, OutputStream out ){
        this.out = new PrintStream( out );
        evalID = id;
    }
    
    
    public static XMLResultLogger getInstance( String runID ){
        if( GLOBAL_LOGGER == null ){
         
            try {
                GLOBAL_LOGGER = new XMLResultLogger( runID, new FileOutputStream(new File("./"+runID+".log" ) ) );
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        return GLOBAL_LOGGER;
    }

    public String getEvaluationID(){
        return evalID;
    }
    
    public void log(AuditEvent evt, HttpResponse response)
    {
        long ts = System.currentTimeMillis();
        StringBuffer s = new StringBuffer();
        
        s.append("<Result auditId=\""+evt.getEventId()+"\" tstamp=\""+ts+"\">\n");
        s.append("<Request method=\""+evt.get( ModSecurity.REQUEST_METHOD ) + "\" url=\""+evt.get( ModSecurity.REQUEST_URI )+"\" />\n");
        s.append("<Response status=\"" + response.getStatus() + "\">\n");

        if( response.getBodyAsString().length() > 0 ){
            s.append("<Body>\n");
            s.append(response.getBodyAsString() );
            s.append("</Body>\n");
        }
        s.append("</Response>\n");
        s.append("</Result>");
        
        out.println( s.toString() );
    }
    
    public void flush(){
        out.flush();
    }
}
