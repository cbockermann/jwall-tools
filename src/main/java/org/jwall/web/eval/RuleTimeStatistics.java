/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.eval;

import java.io.Serializable;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;

import org.jwall.tools.RuleTimes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.data.Statistics;
import stream.util.URLUtilities;

public class RuleTimeStatistics extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(RuleTimeStatistics.class);
	String pivot = "rule:id";
	boolean printTable = false;
	Map<String, Statistics> aggregates = new LinkedHashMap<String, Statistics>();

	@Override
	public Data process(Data input) {

		Serializable piv = input.get(pivot);
		if (piv != null) {

			String group = piv.toString();
			Statistics st = aggregates.get(group);
			if (st == null) {
				st = new Statistics();
				aggregates.put(group, st);
			}

			st.add("rule:invocations", 1.0d);

			for (String key : input.keySet()) {
				if (key.startsWith("rule:") && !key.equals(pivot)) {
					try {
						Double value = new Double(input.get(key) + "");
						st.add(key, value);
					} catch (Exception e) {
					}
				}
			}
		}

		return input;
	}

	@Override
	public void finish() throws Exception {
		super.finish();

		if (!printTable)
			return;

		URL url = RuleTimes.class.getResource("/rule-times-info.txt");
		System.out.println(URLUtilities.readContentOrEmpty(url));

		System.out
				.println("\t+-----------+-------------+--------------+----------------+------------+");
		System.out.printf("\t|%10s | %10s |  %10s  | %10s |%10s  |", "Rule Id",
				"Invocations", "total time", "transformation", "operator");
		System.out.println();
		System.out
				.println("\t+-----------+-------------+--------------+----------------+------------+");

		String fmt = "\t|%10s |   %8d  |    %8.3f  |     %8.3f   |   %8.3f |";

		for (String group : aggregates.keySet()) {
			Statistics st = aggregates.get(group);
			Double d = st.get("rule:invocations");
			st = st.divideBy(d);
			st.put("rule:invocations", d);

			System.out.printf(fmt, group, d.intValue(), st.get("rule:time"),
					st.get("rule:transformationTime"),
					st.get("rule:operatorTime"));
			System.out.println();
		}
		System.out
				.println("\t+-----------+-------------+--------------+----------------+------------+\n\n");
	}

	public Map<String, Statistics> getStatistics() {
		return aggregates;
	}

	public boolean isPrintTable() {
		return printTable;
	}

	public void setPrintTable(boolean printTable) {
		this.printTable = printTable;
	}
}
