/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.eval;

import java.io.File;
import java.io.FileReader;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.Endpoint;


@WebService
@SOAPBinding(style=SOAPBinding.Style.RPC)
public class TestCaseStorage
{

    static File store = new File( "/www/tc-storage/data" );




    public HttpTestCase getById( String id ){


        try {

            File file = new File( store.getAbsolutePath() + File.separator + id );

            if( ! file.exists() || ! file.canRead() )
                return null;


            HttpTestCase htc = (HttpTestCase) TestSet.getXStream().fromXML( new FileReader( file ) );

            return htc;
            
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
    
    public static void main( String args[] )
        throws Exception
    {
        
        
        TestCaseStorage tss = new TestCaseStorage();
        Endpoint ep = Endpoint.publish( "http://localhost:8080/tcs", tss );
        if( ep != null )
            System.out.println( "Success!" );
    }
}
