/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.eval;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.io.StringReader;
import java.net.ConnectException;
import java.net.Socket;
import java.nio.channels.Channels;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Logger;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.eval.transform.Transformation;
import org.jwall.web.http.HttpHeader;
import org.jwall.web.http.nio.HttpResponseChannel;


/**
 * 
 * The TestClient class defines a client that can be used to fire a list of events
 * against some specified target webserver. The events to be sent can be preprocessed
 * for substituting authorization/session-ids etc.
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class EvaluationClient
extends Thread
{
    private static Logger log = Logger.getLogger( "TestClient" );
    TreeSet<AuditEvent> events;
    LinkedList<Transformation> xfrms;
    Hashtable<String,Socket> connections = new Hashtable<String,Socket>();
    long currentTime = 0;
    double speedup = 0.0d;
    long txTime = 0L;
    long sent = 0L;
    protected boolean createFakeSession = false;
    public static long rTime = 1L;
    Hashtable<String,Long> conUsage = new Hashtable<String,Long>();

    /**
     * 
     * Creates a new instance of this class with an
     * empty list of events and no tranformations.
     *
     */
    public EvaluationClient(){
        xfrms = new LinkedList<Transformation>();
    }


    /**
     * This method add the given collection of events to the internal
     * event-queue. The new events are inserted into the queue according
     * to their date (ascending).
     * 
     * @param evts Collection of events to add.
     */
    public void addEvents(Collection<AuditEvent> evts){
        events.addAll( evts );
        log.fine("got new events, have "+events.size()+" now");
    }


    /**
     * This method returns the start-date of the test-clients run.
     * 
     * @return The date of the first event in the event-queue.
     */
    public Date getStartDate(){
        if( events.size() == 0 )
            return null;

        return events.first().getDate();
    }

    public void createFakeSessions( boolean b ){
        this.createFakeSession = b;
    }


    /**
     * This method returns the end-date of the test-clients run.
     * 
     * @return The date of the last event in the event-queue.
     */
    public Date getEndDate(){
        if( events.size() == 0 )
            return null;

        return events.last().getDate();
    }

    public long getTimeSpan(){
        if(events.size() > 0)
            return getEndDate().getTime() - getStartDate().getTime();

        return 0;
    }


    /**
     * This method returns a list of the transformations that are applied
     * to all audit-events before they are injected into the webserver. 
     * 
     * @return List of transformations to be applied to audit-events.
     */
    public List<Transformation> getTransformations(){
        return xfrms;
    }


    /**
     * This method returns the sorted set of audit-events that are waiting
     * for their injection into a webserver.
     * 
     * @return
     */
    public SortedSet<AuditEvent> getPending(){
        return events;
    }


    public TestResult injectEvent( HttpTestCase e, String dst, Integer dport ) 
    throws Exception
    {
        //log.finest("   Evaluating test-case: " + e.getId() );
        //
        // first the list of transformations is applied to
        // the auditevent
        //

        //log.info( "test-case:\n" + TestSet.getXStream().toXML( e ) );
        //log.info("Applying transformations...");


        TestResult result = new TestResult( e.getId() );

        long start = System.currentTimeMillis();
        //for(Transformation t : xfrms)
        //    evt = t.transform( evt );

        txTime += (System.currentTimeMillis() - start);



        //
        // the next step is to create a tcp-connection and
        // write the request into it.
        //

        String conId = System.currentTimeMillis() + ""; //evt.get( ModSecurity.REMOTE_ADDR )+":"+evt.get( ModSecurity.REMOTE_PORT )+"-"+dst+":"+dport;
        Socket srv = null;

        try {
            if( connections.get(conId) != null && connections.get(conId).isConnected() && (! connections.get(conId).isClosed())){
                log.fine("Re-using existing tcp-connection["+conId+"]...");
                srv = connections.get( conId );
                touch( conId );
            } else {
                int port = 80;
                srv = new Socket( dst, port);
                log.fine("Creating new tcp-connection["+conId+"] to "+dst+":"+dport);
            }
        } catch (ConnectException ce) {
            //
            // if this happens, then either the server has dropped our connection or is not
            // even running. the first case may be due to ModSecurity correctly (?) blocking
            // the request, the later is an error preventing the proper evaluation.
            //
            result.getMessages().add( new Message( "Connection faild. Possible refused!?" ) );
            return result;
        }

        long rs = System.currentTimeMillis();
        PrintStream out = new PrintStream( srv.getOutputStream() );
        //out.println( event.getSection( ModSecurity.SECTION_REQUEST_HEADER ) );

        boolean isPost = e.getRequestHeader().startsWith("POST"); //.equals( evt.get( ModSecurity.REQUEST_METHOD ) );

        if( isPost ){
            //
            // TODO: if no post-body is available (i.e. not present in audit-event) we need to do a special
            //       handling since we cannot precisely inject that request
            //
            //System.err.println("POST-request, body is:");
            //System.err.println( evt.getRequestBody() );
            //System.out.println("----------------------");
        }

        char[] content = new char[0];
        if( e.getRequestBody() != null )
            content = e.getRequestBody().toCharArray();

        int cs = content.length;

        BufferedReader r = new BufferedReader( new StringReader( e.getRequestHeader() ) );
        String line = null;
        StringBuffer header = new StringBuffer();
        boolean csInserted = false;
        boolean hasSessionCookie = false;
        do {
            line = r.readLine();
            hasSessionCookie = hasSessionCookie || (line != null && line.startsWith("Cookie: ") && line.indexOf( "JSESSIONID") > 0 );

            if(line != null && line.startsWith("Content-Length:") && cs >= 0){
                line = "Content-Length: "+cs;
                csInserted = true;
            }

            if((line == null || "".equals(line)) && (isPost && !csInserted && cs >= 0)){
                String csline = "Content-Length: "+cs + HttpHeader.CRLF ;
                //System.err.println("Inserting cs-line: "+csline);
                header.append( csline );
                csInserted = true;
            }

            if(line != null && !"".equals(line)){
                header.append(line + HttpHeader.CRLF );
            }


        } while(line != null);


        out.print(header.toString());
        out.print( HttpHeader.CRLF );

        if( cs  > 0 )
            out.print( content );

        out.flush();
        
        start = System.currentTimeMillis();
        
        Map<String,String> received = new HashMap<String,String>();
        
        HttpResponseChannel res = new HttpResponseChannel( Channels.newChannel( srv.getInputStream() ) );

        org.jwall.web.http.HttpResponse response = res.readMessage();
        
        long end = System.currentTimeMillis();
        
        result.add( new Message( Message.STATUS_INFO, "Response took " + (end-start) + " ms." ) );
        
        //log.info( "Response-Header: \n" + response.getHeader() );
        
        String[] re = new String[]{ response.getHeader(), response.getBodyAsString() };

        BufferedReader rr = new BufferedReader(new StringReader( response.getHeader() ) );
        String rl = "";
        do {
            rl = rr.readLine();
            if( rl != null ){
                if( rl.matches("^HTTP/1\\.\\d.*") ){
                    String[] tl = rl.split(" ");
                    if( tl.length < 3 || tl.length > 4 ){
                        result.add( new Message( Message.STATUS_ERROR, "Weird response line from server: \"" + rl + "\"!" ) );
                    } else {
                        received.put( ModSecurity.RESPONSE_PROTOCOL, tl[0] );
                        received.put( ModSecurity.RESPONSE_STATUS, tl[1] );
                        received.put( ModSecurity.RESPONSE_LINE, rl );
                    }
                }

                if( rl.startsWith( "Location" ) ){
                    String redir = rl.replaceFirst(".*: ", "");
                    
                    if( received.containsKey( ModSecurity.RESPONSE_HEADERS + ":Location" ) ){
                        result.add( new Message( Message.STATUS_ERROR, "Found a second Location-header: \"" + rl + "\"") );
                    } else 
                        received.put( ModSecurity.RESPONSE_HEADERS + ":Location", redir );
                }
            }
        } while ( rl != null );

        
        for( Expectation exp : e.getExpectations() ){
            
            if( received.get( exp.getVariable() ) == null ){
                
                result.add( new Message( Message.STATUS_FAILED, "Variable not present: unable to check variable " + exp.getVariable() + " against value '" + exp.getValue() + "'" ) );
                
            } else if ( !received.get( exp.getVariable() ).equals( exp.getValue() ) ) {
                
                result.add( new Message( Message.STATUS_FAILED, "Variable " + exp.getVariable() + " has unexpected value '" + received.get( exp.getVariable() ) + "'" ) );
                
            }
        }

        result.setStatus( TestResult.TEST_OK );
        
        res.close();
        long responding = System.currentTimeMillis() - rs;
        rTime += responding;

        if( re[0].indexOf("Connection: close") > 0 || re[0].indexOf("Connection: Keep-Alive") < 0){
            log.info("Closing connection ["+conId+"] now...");

            out.close();

            srv.close();
            connections.remove( conId );
        } else {
            //log.info("Connection ["+conId+"] is to be kept alive...");
            if(connections.get(conId) == null){
                //log.info("saving socket for later use...");
                connections.put( conId, srv);
            }
        }
        sent++;


        if( result.getStatus().equals( TestResult.TEST_INCOMPLETE ) )
            result.setStatus( TestResult.TEST_OK );

        return result;
    }


    public long eventsSent(){
        return sent;
    }



    public void run(){


        while( ! events.isEmpty() ){
            //log.info("Injecting all "+events.size()+" events now...");
            AuditEvent next = events.first();
            events.remove( next );

            if( currentTime > 0 && speedup > 0.0){
                Double delay =  speedup * (new Double(next.getDate().getTime() - currentTime));
                log.info("need to delay injection for "+delay+" ms");
                try {
                    Thread.sleep( delay.longValue() );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } 

            try {
                //injectEvent( next );
            } catch (Exception e) {
                //e.printStackTrace();
            }
            currentTime = next.getDate().getTime();
        }

        for(Socket s: connections.values() ){
            try {
                s.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void timeOut(String conId){
        Socket s = connections.get( conId );
        if(s != null){
            try {
                s.close();
                connections.remove( conId );
                log.info("Closing "+conId+" due to timeout.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void touch(String conId){
        Long l = conUsage.get(conId);
        if(l != null){
            l = System.currentTimeMillis();
        } else {
            conUsage.put( conId, new Long(System.currentTimeMillis()) );
        }
    }


    public void closeAll()
    {
        synchronized (connections) {
            log.info(connections.keySet().size()+" connections still open, closing them now!");
            Set<String> toRemove = new TreeSet<String>();
            for( String key: connections.keySet() ){
                try {
                    Socket s = connections.get( key );
                    if( s != null ){
                        s.close();
                        toRemove.add( key );
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            for( String k : toRemove ){
                connections.remove( k );
            }
        }
    }

    class Timer extends Thread {
        private Hashtable<String,Long> list;
        long TIME_OUT = 100000;
        EvaluationClient client;

        public Timer(EvaluationClient cl, Hashtable<String,Long> l){
            list = l;
            client = cl;
        }

        public void run(){

            while( true ) {
                System.out.println("Checking for timed-out connections...");
                for(String s : list.keySet()){
                    long x = System.currentTimeMillis() - list.get(s);
                    if( x > TIME_OUT ){
                        System.err.println("connection["+s+"] hasn't been used for "+x+" ms");
                        client.timeOut( s );
                    }
                }

                try {
                    Thread.sleep(20000);
                } catch (Exception e) {}
            }
        }

    }

    public static String md5( String s ){
        String plainText = s;
        MessageDigest mdAlgorithm;
        StringBuffer hexString = new StringBuffer();

        try {
            mdAlgorithm = MessageDigest.getInstance("MD5");
            mdAlgorithm.update(plainText.getBytes());
            byte[] digest = mdAlgorithm.digest();

            for (int i = 0; i < digest.length; i++) {
                plainText = Integer.toHexString(0xFF & digest[i]);

                if (plainText.length() < 2) {
                    plainText = "0" + plainText;
                }

                hexString.append(plainText);
            }
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }

        return hexString.toString();
    }
}
