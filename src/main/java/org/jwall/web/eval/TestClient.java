/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.eval;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.io.StringReader;
import java.net.Socket;
import java.nio.channels.Channels;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.jwall.util.MultiSet;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.eval.transform.Transformation;
import org.jwall.web.eval.transform.TransformationException;
import org.jwall.web.http.HttpHeader;
import org.jwall.web.http.nio.HttpResponseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * The TestClient class defines a client that can be used to fire a list of
 * events against some specified target webserver. The events to be sent can be
 * preprocessed for substituting authorization/session-ids etc.
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class TestClient extends Thread {
	private static Logger log = LoggerFactory.getLogger(TestClient.class);
	TreeSet<AuditEvent> events;
	LinkedList<Transformation> xfrms;
	Hashtable<String, Socket> connections = new Hashtable<String, Socket>();
	long currentTime = 0;
	double speedup = 0.0d;
	long txTime = 0L;
	long sent = 0L;
	protected boolean createFakeSession = false;
	public static long rTime = 1L;
	Hashtable<String, Long> conUsage = new Hashtable<String, Long>();
	Hashtable<String, MultiSet<String>> pageStati = new Hashtable<String, MultiSet<String>>();
	MultiSet<String> stati;
	MultiSet<String> redirects;

	ResultLogger rlog;

	/**
	 * 
	 * Creates a new instance of this class with an empty list of events and no
	 * tranformations.
	 * 
	 */
	public TestClient(ResultLogger rlog) {
		events = new TreeSet<AuditEvent>();
		xfrms = new LinkedList<Transformation>();
		stati = new MultiSet<String>();
		redirects = new MultiSet<String>();
		this.rlog = rlog;
	}

	/**
	 * This method add the given collection of events to the internal
	 * event-queue. The new events are inserted into the queue according to
	 * their date (ascending).
	 * 
	 * @param evts
	 *            Collection of events to add.
	 */
	public void addEvents(Collection<AuditEvent> evts) {
		events.addAll(evts);
		log.debug("got new events, have {} now", events.size());
	}

	/**
	 * This method returns the start-date of the test-clients run.
	 * 
	 * @return The date of the first event in the event-queue.
	 */
	public Date getStartDate() {
		if (events.size() == 0)
			return null;

		return events.first().getDate();
	}

	public void createFakeSessions(boolean b) {
		this.createFakeSession = b;
	}

	/**
	 * This method returns the end-date of the test-clients run.
	 * 
	 * @return The date of the last event in the event-queue.
	 */
	public Date getEndDate() {
		if (events.size() == 0)
			return null;

		return events.last().getDate();
	}

	public long getTimeSpan() {
		if (events.size() > 0)
			return getEndDate().getTime() - getStartDate().getTime();

		return 0;
	}

	/**
	 * This method returns a list of the transformations that are applied to all
	 * audit-events before they are injected into the webserver.
	 * 
	 * @return List of transformations to be applied to audit-events.
	 */
	public List<Transformation> getTransformations() {
		return xfrms;
	}

	/**
	 * This method returns the sorted set of audit-events that are waiting for
	 * their injection into a webserver.
	 * 
	 * @return
	 */
	public SortedSet<AuditEvent> getPending() {
		return events;
	}

	public void injectEvent(AuditEvent e) throws Exception {
		log.debug("   TARGET: {}", e.get(ModSecurity.REQUEST_LINE));
		//
		// first the list of transformations is applied to
		// the auditevent
		//
		AuditEvent evt = e;

		try {
			log.debug("Applying transformations...");

			long start = System.currentTimeMillis();
			for (Transformation t : xfrms) {
				log.debug("apply xfrm {}", t);
				evt = t.transform(evt);
			}

			txTime += (System.currentTimeMillis() - start);

		} catch (TransformationException te) {
			throw new Exception(te.getMessage());
		}

		//
		// the next step is to create a tcp-connection and
		// write the request into it.
		//

		String dst = evt.get(ModSecurity.SERVER_ADDR);
		String dport = evt.get(ModSecurity.SERVER_PORT);
		String conId = evt.get(ModSecurity.REMOTE_ADDR) + ":"
				+ evt.get(ModSecurity.REMOTE_PORT) + "-" + dst + ":" + dport;
		Socket srv = null;
		if (connections.get(conId) != null
				&& connections.get(conId).isConnected()
				&& (!connections.get(conId).isClosed())) {
			log.debug("Re-using existing tcp-connection[{}]...", conId);
			srv = connections.get(conId);
			touch(conId);
		} else {
			int port = Integer.parseInt(evt.get(ModSecurity.SERVER_PORT));
			srv = new Socket(evt.get(ModSecurity.SERVER_ADDR), port);
			log.debug("Creating new tcp-connection[{}] to {}:" + dport, conId,
					dst);
		}

		long rs = System.currentTimeMillis();
		PrintStream out = new PrintStream(srv.getOutputStream());
		// out.println( event.getSection( ModSecurity.SECTION_REQUEST_HEADER )
		// );

		boolean isPost = "POST".equals(evt.get(ModSecurity.REQUEST_METHOD));

		if (isPost) {
			//
			// TODO: if no post-body is available (i.e. not present in
			// audit-event) we need to do a special
			// handling since we cannot precisely inject that request
			//
			// System.err.println("POST-request, body is:");
			// System.err.println( evt.getRequestBody() );
			// System.out.println("----------------------");
		}

		char[] content = evt.get(ModSecurity.REQUEST_BODY).trim().toCharArray();
		int cs = content.length;

		String page = evt.get(ModSecurity.REQUEST_FILENAME);
		MultiSet<String> pStatus = pageStati.get(page);
		if (pStatus == null) {
			pStatus = new MultiSet<String>();
			pageStati.put(page, pStatus);
		}
		BufferedReader r = new BufferedReader(new StringReader(
				evt.get(ModSecurity.REQUEST_HEADER)));
		String line = null;
		StringBuffer header = new StringBuffer();
		boolean csInserted = false;
		boolean hasSessionCookie = false;
		do {
			line = r.readLine();
			hasSessionCookie = hasSessionCookie
					|| (line != null && line.startsWith("Cookie: ") && line
							.indexOf("JSESSIONID") > 0);

			if (line != null && line.startsWith("Content-Length:") && cs >= 0) {
				line = "Content-Length: " + cs;
				csInserted = true;
			}

			if ((line == null || "".equals(line))
					&& (isPost && !csInserted && cs >= 0)) {
				String csline = "Content-Length: " + cs + HttpHeader.CRLF;
				System.err.println("Inserting cs-line: " + csline);
				header.append(csline);
				csInserted = true;
			}

			if (line != null && !"".equals(line)) {
				header.append(line + HttpHeader.CRLF);
			}

		} while (line != null);

		if (createFakeSession && !hasSessionCookie) {
			// System.out.println("Adding fake-session-id");
			String sid = evt.get(ModSecurity.REMOTE_ADDR)
					+ evt.get(ModSecurity.REQUEST_HEADER + ":" + "User-Agent");
			header.append("Cookie: JSESSIONID=" + md5(sid).toUpperCase() + "\n");
		} else {
			if (hasSessionCookie) {
				// System.out.println("SessionCookie already present.");
			}
		}

		// System.err.print("header to send:\n"+header.toString());
		// System.err.println( HttpHeader.CRLF );
		out.print(header.toString());
		out.print(HttpHeader.CRLF);

		// out.println();

		if (cs > 0) { // ! "".equals( content ) )
			// System.err.println("body to send:\n" + new String(content) );
			out.print(content);
		}

		out.flush();
		HttpResponseChannel res = new HttpResponseChannel(
				Channels.newChannel(srv.getInputStream()));

		org.jwall.web.http.HttpResponse response = res.readMessage();
		String[] re = new String[] { response.getHeader(),
				response.getBodyAsString() };
		log.trace("Response Header:\n{}", re[0]);

		rlog.log(evt, response);

		BufferedReader rr = new BufferedReader(new StringReader(re[0]));
		String rl = "";
		do {
			rl = rr.readLine();

			if (rl != null && rl.matches("^HTTP/1\\.\\d.*")) {
				String[] tl = rl.split(" ");
				if (tl.length > 1) {
					stati.add(tl[1]);
				}
			}

			if (rl != null && rl.startsWith("Location")) {
				String redir = rl.replaceFirst(".*: ", "");
				redirects.add(redir);
				pStatus.add(redir);
			}
		} while (rl != null);

		res.close();
		long responding = System.currentTimeMillis() - rs;
		rTime += responding;

		if (re[0].indexOf("Connection: close") > 0
				|| re[0].indexOf("Connection: Keep-Alive") < 0) {
			log.debug("Closing connection [" + conId + "] now...");

			out.close();

			srv.close();
			connections.remove(conId);
		} else {
			// log.info("Connection ["+conId+"] is to be kept alive...");
			if (connections.get(conId) == null) {
				// log.info("saving socket for later use...");
				connections.put(conId, srv);
			}
		}
		sent++;
	}

	public long eventsSent() {
		return sent;
	}

	public String getStatusMap() {
		StringBuffer s = new StringBuffer("\n");

		for (String st : stati.getValues())
			s.append("   -> " + st + "  #" + stati.getCount(st) + "\n");

		s.append("\nRedirects to:\n");

		for (String v : redirects.getValues())
			s.append("   -> " + v + "  #" + redirects.getCount(v) + "\n");

		s.append("\n\n-------------------------------------------------------------------\nPage Status Summary:\n");

		for (String page : pageStati.keySet()) {
			MultiSet<String> m = pageStati.get(page);
			if (m.size() > 0) {
				s.append(page + ":  " + m);
				// for( String status : m.getValues() )
				// s.append( "\t\""+status+"\"  # "+m.getCount( status ) + "\n"
				// );
				s.append("\n");
			}
		}

		return s.toString();
	}

	public Hashtable<String, MultiSet<String>> getPageStati() {
		return pageStati;
	}

	public void setPageStati(Hashtable<String, MultiSet<String>> pageStati) {
		this.pageStati = pageStati;
	}

	public MultiSet<String> getStati() {
		return stati;
	}

	public void setStati(MultiSet<String> stati) {
		this.stati = stati;
	}

	public MultiSet<String> getRedirects() {
		return redirects;
	}

	public void setRedirects(MultiSet<String> redirects) {
		this.redirects = redirects;
	}

	public void run() {

		while (!events.isEmpty()) {
			// log.info("Injecting all "+events.size()+" events now...");
			AuditEvent next = events.first();
			events.remove(next);

			if (currentTime > 0 && speedup > 0.0) {
				Double delay = speedup
						* (new Double(next.getDate().getTime() - currentTime));
				log.info("need to delay injection for " + delay + " ms");
				try {
					Thread.sleep(delay.longValue());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				injectEvent(next);
			} catch (Exception e) {
				e.printStackTrace();
				log.info("Failed to re-inject event:\n{}", next);
			}
			currentTime = next.getDate().getTime();
		}

		for (Socket s : connections.values()) {
			try {
				s.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	protected void timeOut(String conId) {
		Socket s = connections.get(conId);
		if (s != null) {
			try {
				s.close();
				connections.remove(conId);
				log.info("Closing " + conId + " due to timeout.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	protected void touch(String conId) {
		Long l = conUsage.get(conId);
		if (l != null) {
			l = System.currentTimeMillis();
		} else {
			conUsage.put(conId, new Long(System.currentTimeMillis()));
		}
	}

	public void closeAll() {
		synchronized (connections) {
			log.debug(connections.keySet().size()
					+ " connections still open, closing them now!");
			Set<String> toRemove = new TreeSet<String>();
			for (String key : connections.keySet()) {
				try {
					Socket s = connections.get(key);
					if (s != null) {
						s.close();
						toRemove.add(key);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			for (String k : toRemove) {
				connections.remove(k);
			}
		}
	}

	class Timer extends Thread {
		private Hashtable<String, Long> list;
		long TIME_OUT = 100000;
		TestClient client;

		public Timer(TestClient cl, Hashtable<String, Long> l) {
			list = l;
			client = cl;
		}

		public void run() {

			while (true) {
				System.out.println("Checking for timed-out connections...");
				for (String s : list.keySet()) {
					long x = System.currentTimeMillis() - list.get(s);
					if (x > TIME_OUT) {
						System.err.println("connection[" + s
								+ "] hasn't been used for " + x + " ms");
						client.timeOut(s);
					}
				}

				try {
					Thread.sleep(20000);
				} catch (Exception e) {
				}
			}
		}

	}

	public static String md5(String s) {
		String plainText = s;
		MessageDigest mdAlgorithm;
		StringBuffer hexString = new StringBuffer();

		try {
			mdAlgorithm = MessageDigest.getInstance("MD5");
			mdAlgorithm.update(plainText.getBytes());
			byte[] digest = mdAlgorithm.digest();

			for (int i = 0; i < digest.length; i++) {
				plainText = Integer.toHexString(0xFF & digest[i]);

				if (plainText.length() < 2) {
					plainText = "0" + plainText;
				}

				hexString.append(plainText);
			}
		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
		}

		return hexString.toString();
	}
}
