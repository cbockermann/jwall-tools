/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.eval;

import java.util.LinkedHashMap;
import java.util.Set;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;

public class TestEvent
{
    String requestHeader;
    
    String requestBody;
    
    LinkedHashMap<String,String> expects = new LinkedHashMap<String,String>();

    
    
    
    public TestEvent( AuditEvent evt ){
        
        requestHeader = evt.get( ModSecurity.REQUEST_HEADER );
        
        requestBody = evt.get( ModSecurity.REQUEST_BODY );

        
        String[] expectVars = new String[]{
                ModSecurity.RESPONSE_STATUS,
                ModSecurity.RESPONSE_HEADERS + ":Location",
                ModSecurity.RESPONSE_PROTOCOL
        };
        
        for( String var : expectVars )
            if( evt.isSet( var ) )
                expects.put( var, evt.get( var ) );
    }
    
    
    public Set<String> getExpectedVariables(){
        return expects.keySet();
    }
    
    public String getExpected( String key ){
        return expects.get( key );
    }
    
    public void setExpected( String key, String val ){
        expects.put( key, val );
    }
}
