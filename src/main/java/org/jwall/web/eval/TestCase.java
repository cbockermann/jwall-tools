/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.eval;

import java.util.List;
import java.util.Set;


/**
 * This interface defines the basic functions common to all test cases.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface TestCase
{
    /**
     * Every test case should provide an unique identifier by its own.
     * 
     * @return
     */
    public String getId();
    
    
    /**
     * The title of a test case is meant to provide a short (at most one-line) comment to
     * present the intention of this case to the user. E.g. &quot;XSS attack vector 42&quot;.
     * 
     * @return The title string.
     */
    public String getName();
    
    
    /**
     * This method sets the title of a test case instance.
     * 
     * @param title The new title.
     */
    public void setName( String title );
    
    
    /**
     * Returns the comment for this test case. This may be a small description of why this
     * test-case has been created.
     * 
     * @return
     */
    public String getComment();
    
    
    /**
     * This method provides a comment for this test case. The use of comments is recommended
     * to keep track of the cases intentions.
     * 
     * @param comment The comment string.
     */
    public void setComment( String comment );
    
    
    public Set<String> getTags();
    
    
    public void setTags( Set<String> tags );
    
    
    public List<Expectation> getExpectations();
    
    
    public void setResult( TestResult result );
}
