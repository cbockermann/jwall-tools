/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.sensor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;

public class TCPIndexListener extends IndexListener {
	final static Logger log = LoggerFactory.getLogger(UDPIndexListener.class);
	final ServerSocket socket;

	public TCPIndexListener(LinkedBlockingQueue<Data> queue, String address,
			int port) throws Exception {
		super(queue);
		socket = new ServerSocket(port, 1000, InetAddress.getByName(address));
		setDaemon(true);
	}

	public void run() {
		log.info("Starting to receive events on socket {}", socket);
		while (true) {
			try {
				Socket client = socket.accept();
				log.info("Starting new client handler thread for socket {}...",
						client);
				new TCPHandler(queue, client).start();
			} catch (Exception e) {
				log.error("Error while receiving: {}", e.getMessage());
				if (log.isDebugEnabled()) {
					e.printStackTrace();
				}
				doSleep(1000);
			}
		}
	}

	public class TCPHandler extends Thread {
		final Logger log = LoggerFactory.getLogger(TCPHandler.class);
		LinkedBlockingQueue<Data> queue;
		Socket socket;
		final BufferedReader reader;

		public TCPHandler(LinkedBlockingQueue<Data> queue, Socket socket)
				throws Exception {
			this.queue = queue;
			this.socket = socket;
			// this.socket.setSoTimeout(10000);
			reader = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
		}

		public void run() {
			try {
				String line = reader.readLine();
				while (line != null) {
					log.debug("Read line: {}", line);
					queue.add(parseLine(line));
					line = reader.readLine();

					if (queue.size() % 100 == 0) {
						log.debug("{} lines read", queue.size());
					}
				}
				log.debug("tcp handler finished...");
			} catch (Exception e) {
				log.error(
						"Failed to read index entry from client connection: {}",
						e.getMessage());
				if (log.isDebugEnabled()) {
					e.printStackTrace();
				}
			}
		}
	}
}