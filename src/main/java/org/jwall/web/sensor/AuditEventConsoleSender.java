/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.sensor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.URI;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.jwall.Collector;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.ConcurrentAuditWriter;
import org.jwall.web.audit.util.Base64Codec;
import org.jwall.web.audit.util.MD5;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.util.URLUtilities;

/**
 * 
 * This class implements a simple socket-handler which provides easy injection
 * of audit-events to the modsecurity-console.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class AuditEventConsoleSender implements AuditEventListener {

	/** A unique logger for this class */
	private static Logger log = LoggerFactory
			.getLogger(AuditEventConsoleSender.class);

	/** the uri of the receiver servlet */
	public final static String CONSOLE_URI = "/rpc/auditLogReceiver";

	private final URI consoleUri;

	/** the destination host */
	private String host = "localhost";

	/** the port to which this senders connects */
	private int port = 8888;

	/** the user name for authentication */
	private String user = "";

	/** the password used for authentication */
	private String pass = "";

	private Socket socket = null;

	boolean keepAlive = true;

	/**
	 * 
	 * This method creates a new console sender that sends all arriving events
	 * to the given host <code>host</code> using <code>login</code> and
	 * <code>password</code> for authentifaction.
	 * 
	 * @param host
	 *            The host on which the Console is running.
	 * @param port
	 *            The port, at which the Console is listening.
	 * @param login
	 *            User-name for authentication with the console.
	 * @param password
	 *            Password for authentication.
	 * 
	 */
	public AuditEventConsoleSender(URI consoleUri, String login, String password) {
		this.consoleUri = consoleUri;
		this.host = consoleUri.getHost();

		if ("http".equalsIgnoreCase(consoleUri.getScheme())) {
			this.port = 80;
		}

		if ("https".equalsIgnoreCase(consoleUri.getScheme())) {
			this.port = 443;
		}

		if (consoleUri.getPort() > 0) {
			port = consoleUri.getPort();
		}

		this.user = login;
		this.pass = password;

		try {
			log.debug("Disabling certificate validation...");
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, new TrustManager[] { new ZeroTrustManager() },
					new java.security.SecureRandom());
			HttpsURLConnection
					.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method sends the given audit-event to the configured console.
	 * 
	 * @param evt
	 *            The event to be sent to the console.
	 * @throws Exception
	 *             In case an error occurs.
	 */
	public String sendAuditEvent(AuditEvent evt) throws Exception {

		final String txId = evt.get(ModSecurity.TX_ID);
		log.debug("Sending audit event {}", txId);

		final byte[] data = evt.toString().getBytes();
		final String hash = "md5:" + MD5.md5(data);

		String sum = ConcurrentAuditWriter.createSummary(evt);
		Base64Codec codec = new Base64Codec();
		String cred = new String(codec.encode((user + ":" + pass).getBytes()));
		String ua = "jwall.org/Collector Version " + Collector.VERSION;
		Socket sock = this.getSocketConnection();

		try {

			StringBuffer request = new StringBuffer();

			request.append("PUT " + consoleUri.getPath() + " HTTP/1.1\r\n");
			request.append("Authorization: Basic " + cred + "\r\n");
			request.append("Host: " + consoleUri.getHost());

			if (this.port != 80)
				request.append(":" + port);

			request.append("\r\n");

			request.append("X-Content-Hash: " + hash + "\r\n");
			request.append("X-ForensicLog-Summary: " + sum + "\r\n");
			request.append("User-Agent: " + ua + "\r\n");

			if (keepAlive)
				request.append("Connection: keep-alive\r\n");

			request.append("Content-Length: " + data.length + "\r\n");
			request.append("\r\n");

			log.debug("Sending request header:\n" + request.toString());

			PrintStream out = new PrintStream(sock.getOutputStream());
			out.print(request.toString());

			log.debug("Writing " + data.length + " bytes of data to server...");
			out.write(data);
			out.flush();

		} catch (Exception e) {
			e.printStackTrace();
			sock = null;
			return "ERROR: " + e.getMessage();
		}

		final Map<String, String> header = new LinkedHashMap<String, String>();
		StringBuffer responseHeader = new StringBuffer();
		StringBuffer response = new StringBuffer();
		log.debug("Reading response...");
		BufferedReader r = new BufferedReader(new InputStreamReader(
				sock.getInputStream()));
		String line = r.readLine();
		log.debug("Response line: {}", line);
		header.put("RESPONSE_HEADER", line);
		if (line != null) {
			responseHeader.append(line);
			responseHeader.append("\r\n");
			if (line.toLowerCase().indexOf("200 ok") >= 0) {
				log.debug("Server accepted event.");
				response.append(txId + " ok.");
			} else {
				log.debug("Response-line: " + line);
			}
		}

		log.debug("Reading next line...");
		line = r.readLine();
		log.debug("line: {}", line);
		while (line != null) {
			responseHeader.append(line);
			responseHeader.append("\r\n");

			if (line.trim().isEmpty())
				break;
			else {
				int idx = line.indexOf(": ");
				String key = line.substring(0, idx);
				String value = line.substring(idx + 2);
				header.put(key.toLowerCase(), value);
			}
			line = r.readLine();
		}
		log.debug("header: {}", header);

		Integer contentLength = new Integer(header.get("content-length"));
		if (contentLength > 0) {
			response.append(URLUtilities.readResponse(sock.getInputStream()));
			log.debug("response: {}", response);
		}
		r.close();

		/*
		 * InputStream in = sock.getInputStream(); int avail = in.available();
		 * while( avail > 0 ){ byte[] bytes = new byte[avail]; log.info(
		 * "Consuming " + avail + " bytes of response..." ); in.read( bytes );
		 * avail = in.available(); }
		 */

		if (!keepAlive) {
			closeSocketConnection();
		}
		return response.toString();
	}

	public Socket getSocketConnection() throws Exception {

		if (socket != null && !socket.isClosed())
			return socket;

		log.debug("Establishing socket connection");

		String ssl = "" + (consoleUri.getScheme().equalsIgnoreCase("https"));

		if (ssl != null && !"false".equalsIgnoreCase(ssl)) { // port == 8888 ||
																// port == 8889
																// ){

			log.debug("Creating new ssl-enabled socket to " + host + ":" + port);

			// using secure https connection
			//
			SSLContext context = SSLContext.getInstance("TLS");
			TrustManager[] trustManagers = new TrustManager[] { new ZeroTrustManager() };
			context.init(null, trustManagers, null);

			SSLSocketFactory sf = context.getSocketFactory();
			socket = sf.createSocket(host, port);

		} else {

			log.debug("Creating new plain-http socket to " + host + ":" + port);
			socket = new Socket(host, port);
		}

		return socket;
	}

	public void closeSocketConnection() throws Exception {

		if (socket != null && !socket.isClosed())
			socket.close();

		socket = null;
	}

	/**
	 * 
	 * Simply send all arriving events to the configured console.
	 * 
	 */
	public void eventArrived(AuditEvent evt) {

		try {

			this.sendAuditEvent(evt);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	public void eventsArrived(Collection<AuditEvent> events) {
		for (AuditEvent evt : events)
			eventArrived(evt);
	}

	public class ZeroTrustManager implements X509TrustManager, TrustManager {
		private Logger log = LoggerFactory.getLogger("ZeroTrustManager");

		public void checkClientTrusted(X509Certificate[] chain, String authType)
				throws CertificateException {

			log.debug("checkClientTrusted: \n");

			for (X509Certificate cert : chain) {
				log.debug("-------------------------------------------------------");
				log.debug(" SubjectDN = " + cert.getSubjectDN());
				log.debug(" Issuer = " + cert.getIssuerDN());
			}
		}

		public void checkServerTrusted(X509Certificate[] chain, String authType)
				throws CertificateException {

			log.debug("checkServerTrusted: \n");

			for (X509Certificate cert : chain) {
				log.debug("-------------------------------------------------------");
				log.debug(" SubjectDN = " + cert.getSubjectDN());
				log.debug(" Issuer = " + cert.getIssuerDN());
			}
		}

		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}
	}
}
