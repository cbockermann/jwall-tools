/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.sensor;

import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import stream.Data;
import stream.data.DataFactory;
import stream.util.parser.Parser;
import stream.util.parser.ParserGenerator;

public class IndexListener extends Thread {
	final static String[] fields = new String[] { "sensor.address",
			"receiver.address", "dummy0", "dummy1", "date", "uri", "status",
			"size", "d", "d'", "txId", "path", "offset", "length", "checksum" };

	final static String format = "%(sensor) %(server) - - [%(date)] \"%(method) %(uri) %(protocol)\" %(status) %(size) \"-\" \"-\" %(txId) \"-\" %(path) %(offset) %(size) %(checksum)";
	final LinkedBlockingQueue<Data> queue;
	final Parser<Map<String, String>> parser;

	public IndexListener(LinkedBlockingQueue<Data> queue) {
		this.queue = queue;

		ParserGenerator pg = new ParserGenerator(format);
		parser = pg.newParser();
	}

	public void run() {

	}

	public Data parseLine(String s) throws Exception {
		// String line = s.replaceFirst("\\[", "\"").replaceFirst("\\]", "\"");
		// String[] token = ParserUtils.splitQuotedString(line);
		// token = AccessLogAuditReader.splitAccessLine(line);

		try {
			Map<String, String> values = parser.parse(s);
			Data item = DataFactory.create();
			item.putAll(values);
			return item;
		} catch (Exception e) {
			throw e;
		}
	}

	public void doSleep(long ms) {
		try {
			Thread.sleep(ms);
		} catch (Exception e) {
		}
	}
}