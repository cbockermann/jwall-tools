/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.sensor;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;

public class UDPIndexListener extends IndexListener {
	final Logger log = LoggerFactory.getLogger(UDPIndexListener.class);
	final DatagramSocket socket;

	public UDPIndexListener(LinkedBlockingQueue<Data> queue, String address,
			int port) throws Exception {
		super(queue);
		socket = new DatagramSocket(port, InetAddress.getByName(address));
		setDaemon(true);
	}

	public void run() {
		log.info("Starting to receive events on socket {}", socket);
		while (true) {
			try {

				DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);

				log.info("Waiting for incoming packets...");
				socket.receive(packet);
				log.info("packet received: {}", packet);

				int len = packet.getLength();
				int off = packet.getOffset();
				byte[] data = new byte[len];
				for (int i = 0; i < len; i++) {
					data[i] = packet.getData()[i + off];
				}

				String str = new String(data);
				log.info("Received: {}", str);

				Data item = parseLine(str);
				queue.add(item);

			} catch (Exception e) {
				log.error("Error while receiving: {}", e.getMessage());
				if (log.isDebugEnabled()) {
					e.printStackTrace();
				}
				doSleep(1000);
			}
		}
	}
}