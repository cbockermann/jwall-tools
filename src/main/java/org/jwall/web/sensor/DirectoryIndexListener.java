/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.sensor;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.LinkedBlockingQueue;

import org.jwall.log.io.SequentialFileInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;

/**
 * @author chris
 * 
 */
public class DirectoryIndexListener extends IndexListener {

	static Logger log = LoggerFactory.getLogger(DirectoryIndexListener.class);
	final SequentialFileInputStream stream;

	/**
	 * @param queue
	 */
	public DirectoryIndexListener(LinkedBlockingQueue<Data> queue,
			String pattern) throws IOException {
		super(queue);

		File file = new File(pattern);

		stream = new SequentialFileInputStream(new File(pattern),
				file.getName() + "*", false);

		File dir = file.getParentFile();
		log.info("Directory is: {}", dir);
		log.info("pattern is: '{}'", file.getName() + "*");

	}

	/**
	 * @see org.jwall.web.sensor.IndexListener#run()
	 */
	@Override
	public void run() {

		BufferedReader reader = new BufferedReader(
				new InputStreamReader(stream));

		while (true) {
			try {
				String line = reader.readLine();
				if (line != null) {

					Data event = parseLine(line);
					queue.add(event);

				} else {
					doSleep(100);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}