/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.sensor;

import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.io.AbstractStream;
import stream.io.SourceURL;

public class AuditIndexStream extends AbstractStream {

	static Logger log = LoggerFactory.getLogger(AuditIndexStream.class);
	LinkedBlockingQueue<Data> queue = new LinkedBlockingQueue<Data>();
	String address = "127.0.0.1";
	Integer port = 11005;

	public AuditIndexStream(SourceURL url) {
		super(url);
	}

	@Override
	public void init() throws Exception {
		super.init();

		log.info("SourceURL is: {}", url.toString());

		if (url.getProtocol().startsWith("file")) {
			log.info("Starting directory-index-listener for '{}'",
					url.getFile());
			DirectoryIndexListener listener = new DirectoryIndexListener(queue,
					url.getFile());
			listener.start();
		} else {
			log.info("Starting TCP index listener...");
			TCPIndexListener listener = new TCPIndexListener(queue, address,
					port);
			listener.start();
		}
	}

	@Override
	public Data readNext() throws Exception {
		while (true) {
			try {
				Data item = queue.take();
				if (item != null)
					return item;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}
}
