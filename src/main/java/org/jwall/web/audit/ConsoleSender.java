/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.audit;

import java.io.File;
import java.net.URI;

import org.jwall.web.sensor.AuditEventConsoleSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;

public class ConsoleSender extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(ConsoleSender.class);
	String sensor;
	String password;
	String consoleUri;
	boolean keepEntries = true;

	AuditEventConsoleSender sender;

	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);

		if (consoleUri == null) {
			throw new Exception("No 'consoleUri' property specified for Mlogc!");
		}

		String sensorName = sensor;
		String sensorPassword = password;

		URI uri = new URI(consoleUri);
		log.info("uri.getUserInfo() = {}", uri.getUserInfo());
		if (uri.getUserInfo() != null && !uri.getUserInfo().trim().isEmpty()) {
			String[] tok = uri.getUserInfo().split(":");
			sensorName = tok[0];
			sensorPassword = tok[1];
		}

		if (sensor != null) {
			sensorName = sensor;
		} else {
			sensor = sensorName;
		}

		if (password != null) {
			sensorPassword = password;
		}

		sender = new AuditEventConsoleSender(uri, sensorName, sensorPassword);
	}

	@Override
	public Data process(Data input) {
		log.debug("Processing item {}", input);

		AuditEvent evt = (AuditEvent) input.get("event");
		if (evt == null) {
			log.warn("No AuditEvent loaded for the given entry!");
			return input;
		}

		try {

			String ret = sender.sendAuditEvent(evt);
			log.info("Return: {}", ret);

			evt.set(AuditEvent.SENSOR_NAME, sensor);
			input.put("event", evt);

			if (!keepEntries) {

				File file = new File(input.get(AuditEvent.FILE) + "");
				if (!file.exists()) {
					log.warn("Cannot remove file '{}' - file does not exist!",
							file);
				} else {
					boolean removed = file.delete();
					if (!removed) {
						log.error("Failed to delete file '{}'", file);
					} else {
						log.debug("File '{}' deleted.", file);
					}
				}
			}

		} catch (Exception e) {
			log.error("Failed to process audit event: {}", e.getMessage());
			if (log.isDebugEnabled()) {
				e.printStackTrace();
			}
		}
		return input;
	}

	public String getSensor() {
		return sensor;
	}

	public void setSensor(String sensor) {
		this.sensor = sensor;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConsoleUri() {
		return consoleUri;
	}

	public void setConsoleUri(String consoleUri) {
		this.consoleUri = consoleUri;
	}

	public boolean isKeepEntries() {
		return keepEntries;
	}

	public void setKeepEntries(boolean keepEntries) {
		this.keepEntries = keepEntries;
	}
}
