/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.audit.stats.io;

import java.util.SortedSet;
import java.util.TreeSet;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;

import org.jwall.util.MultiSet;

public class MultiSetUtils {

	public static String toString(MultiSet<String> set) {
		StringBuffer b = new StringBuffer();

		SortedSet<String> keys = sort(set);
		for (String key : keys) {
			if (!key.startsWith("__")) {
				b.append(set.getCount(key));
				b.append("  ");
				b.append(key);
				b.append("\n");
			}
		}

		return b.toString();
	}

	public static String toJSON(MultiSet<String> set) throws Exception {
		JSONObject obj = new JSONObject();
		for (String key : set.getValues()) {
			obj.put(key, set.getCount(key));
		}
		return obj.toString();
	}

	public static MultiSet<String> fromJSON(String js) throws Exception {
		MultiSet<String> ms = new MultiSet<String>();
		JSONParser parser = new JSONParser(JSONParser.MODE_PERMISSIVE);
		JSONObject obj = (JSONObject) parser.parse(js);
		for (String key : obj.keySet()) {
			try {
				String val = obj.get(key) + "";
				ms.addCount(key, new Long(val));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return ms;
	}

	public static SortedSet<String> sort(MultiSet<String> set) {
		TreeSet<String> keys = new TreeSet<String>(new MultiSetOrder(set));
		for (String key : set.getValues())
			keys.add(key);
		return keys;
	}
}
