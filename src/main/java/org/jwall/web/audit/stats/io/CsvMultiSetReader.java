/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.audit.stats.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.jwall.util.MultiSet;

public class CsvMultiSetReader implements MultiSetReader {
	public final static String SEPARATOR = "|";
	BufferedReader r;
	
	
	public CsvMultiSetReader( InputStream in ) throws IOException {
		r = new BufferedReader( new InputStreamReader( in ) );
	}
	
	
	@Override
	public MultiSet<String> readNext() throws IOException {
		
		String line = r.readLine();
		String[] tok = split( line );
		int start = 0;
		MultiSet<String> row = new MultiSet<String>();
		if( tok[start].startsWith( "@" ) ){
			row.addCount( "__TIME__", new Long( tok[start].substring( 1 ) ) );
			start++;
		}
		
		for( int i = start; i < tok.length; i += 2 )
			row.addCount( tok[i], new Long(tok[i+1]));
		
		return row;
	}
	
	protected String[] split( String str ){
		return str.split( "\\" + SEPARATOR );
	}
	
	
	public void close(){
		try {
			r.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
