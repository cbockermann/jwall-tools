/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.audit;

import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import net.minidev.json.JSONObject;

import org.jwall.audit.EventProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import stream.data.DataFactory;

public class Graylog2Sender extends AbstractProcessor implements
		EventProcessor<AuditEvent> {

	static Logger log = LoggerFactory.getLogger(Graylog2Sender.class);
	final Map<String, Object> ctx = new HashMap<String, Object>();

	String address;
	Integer port;

	InetAddress destination;
	DatagramSocket socket;
	final AtomicLong sent = new AtomicLong(0L);

	@Override
	public void init(ProcessContext context) throws Exception {
		socket = new DatagramSocket();
		destination = InetAddress.getByName(address);
	}

	@Override
	public void finish() throws Exception {
		socket.close();
		log.info("{} messages sent to graylog2...", sent.get());
	}

	@Override
	public Data process(Data input) {

		Serializable value = input.get("event");
		if (value != null && (value instanceof AuditEvent)) {
			AuditEvent event = (AuditEvent) value;
			try {
				event = processEvent(event, ctx);
				input.put("event", event);
			} catch (Exception e) {
				log.error("Failed to process event: {}", e.getMessage());
				if (log.isDebugEnabled()) {
					e.printStackTrace();
				}
			}
		}

		return input;
	}

	@Override
	public AuditEvent processEvent(AuditEvent evt, Map<String, Object> context)
			throws Exception {

		final String txId = evt.get(ModSecurity.TX_ID);
		log.debug("Processing event {}", txId);
		AuditEventMessage[] msgs = evt.getEventMessages();
		if (msgs == null) {
			log.debug("Event does not have any event messages attached!");
			return evt;
		}

		log.debug("Event has {} messages", msgs.length);

		for (int i = 0; i < msgs.length; i++) {
			AuditEventMessage msg = msgs[i];

			Data item = DataFactory.create();
			item.put("version", "1.0");
			item.put("host", evt.get(ModSecurity.SERVER_ADDR));
			item.put("short_message", msg.getRuleMsg());
			item.put("full_message", msg.getText());
			item.put("timestamp", evt.getDate().getTime() / 1000.0d);
			item.put("level", msg.getSeverity());
			item.put("facility", "ModSecurity");
			item.put("file", msg.getFile());
			item.put("line", msg.getLine());

			String protoVersion = "__UNDEFINED__";
			String requestLine = evt.get(ModSecurity.REQUEST_LINE);
			if (requestLine != null) {
				String[] tok = requestLine.split("\\s+");
				if (tok.length > 2) {
					protoVersion = tok[2];
				}
			}

			//
			// Message format as requested by Philipp Hellmich
			//
			item.put("_event_txId", txId);
			if (msg.getRuleId() != null) {
				item.put("_rule_id", msg.getRuleId());
			}

			List<String> tags = msg.getRuleTags();
			if (tags != null && !tags.isEmpty()) {
				item.put("_rule_tags", tags.toArray(new String[tags.size()]));
			}

			item.put("_sensor", evt.get(AuditEvent.SENSOR_NAME));
			item.put("_http_method", evt.get(ModSecurity.REQUEST_METHOD));
			item.put("_http_version", protoVersion);
			item.put("_http_response_code",
					evt.get(ModSecurity.RESPONSE_STATUS));

			item.put("_http_request", "/index.php");
			item.put("_http_host",
					evt.get(ModSecurity.REQUEST_HEADERS + ":Host"));

			item.put("_clientip", evt.get(ModSecurity.REMOTE_ADDR));

			emit(item);
		}

		return evt;
	}

	public void emit(Data item) {

		log.debug("Emitting message {}", item);
		try {

			if (socket == null) {
				log.debug("Creating new datagram-socket...");
				socket = new DatagramSocket();
			}

			String json = JSONObject.toJSONString(item) + "\n";
			byte[] data = json.getBytes();

			DatagramPacket packet = new DatagramPacket(data, data.length,
					destination, port);
			log.debug("Sending UDP packet...");
			socket.send(packet);
			sent.incrementAndGet();
		} catch (Exception e) {
			log.error("Failed to emit message: {}", e.getMessage());
			if (log.isDebugEnabled())
				e.printStackTrace();
		}
	}

	public String join(List<String> strs) {
		StringBuffer s = new StringBuffer();
		if (strs == null || strs.isEmpty()) {
			return s.toString();
		}

		Iterator<String> it = strs.iterator();
		while (it.hasNext()) {
			s.append(it.next());
			if (it.hasNext())
				s.append(",");
		}

		return s.toString();
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}
}