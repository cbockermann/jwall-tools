/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.audit;

import java.io.File;
import java.io.FileInputStream;

import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;

public class LoadAuditEvent extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(LoadAuditEvent.class);
	String key = "event";
	File dataDirectory;

	@Override
	public Data process(Data input) {

		String path = input.get("path") + "";
		File file = new File(dataDirectory.getAbsolutePath() + path);
		log.debug("Audit event is expected at {}", file);
		log.debug("   event file exists? {}", file.isFile());

		if (!(file.isFile() && file.canRead())) {
			log.warn("File {} does not exist or cannot be read!", file);
			return input;
		}

		try {
			ModSecurity2AuditReader reader = new ModSecurity2AuditReader(
					new FileInputStream(file));

			AuditEvent evt = reader.readNext();
			reader.close();
			input.put(key, evt);

			input.put(AuditEvent.FILE, file.getAbsolutePath());
			input.put(AuditEvent.FILE_OFFSET, 0);

		} catch (Exception e) {
			log.error("Failed to load event: {}", e.getMessage());
			if (log.isDebugEnabled())
				e.printStackTrace();
		}

		return input;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public File getDataDirectory() {
		return dataDirectory;
	}

	public void setDataDirectory(File dataDirectory) {
		this.dataDirectory = dataDirectory;
	}
}
