/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.web.audit;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import stream.Data;
import stream.Processor;
import stream.data.DataFactory;
import stream.io.Sink;

public class ExtractGELFOutput implements Processor {

	String prefix = "_rule:";
	String key = "event";
	Sink[] output;

	@Override
	public Data process(Data input) {

		Serializable value = input.get(key);
		if (value != null && value instanceof AuditEvent) {

			AuditEvent evt = (AuditEvent) value;
			final String txId = evt.get(ModSecurity.TX_ID);
			final Long timestamp = evt.getTimestamp();

			AuditEventMessage[] msgs = evt.getEventMessages();
			for (int i = 0; i < msgs.length; i++) {
				AuditEventMessage msg = msgs[i];

				Data item = DataFactory.create();
				item.put("version", "1.0");
				item.put("host", evt.get(ModSecurity.REQUEST_HEADERS + ":Host"));
				item.put("short_message", msg.getRuleMsg());
				item.put("full_message", msg.getRuleMsg());
				item.put("timestamp", timestamp.doubleValue() / 1000.0d);
				item.put("level", msg.getSeverity());
				item.put("facility",
						"ModSecurity:" + evt.get(AuditEvent.SENSOR_NAME));
				item.put("file", msg.getFile());
				item.put("line", msg.getLine());

				item.put("_event:txId", txId);
				item.put(prefix + "id", msg.getRuleId());
				List<String> tags = msg.getRuleTags();
				if (tags != null && !tags.isEmpty()) {
					item.put(prefix + "tags", join(msg.getRuleTags()));
				}

				emit(item);
			}

		}

		return null;
	}

	protected void emit(Data item) {
		if (output != null && output.length > 0) {
			for (int i = 0; i < output.length; i++) {
				Sink out = output[i];
				if (out != null) {
					try {
						out.write(item);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	public String join(List<String> strs) {
		StringBuffer s = new StringBuffer();
		if (strs == null || strs.isEmpty()) {
			return s.toString();
		}

		Iterator<String> it = strs.iterator();
		while (it.hasNext()) {
			s.append(it.next());
			if (it.hasNext())
				s.append(",");
		}

		return s.toString();
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Sink[] getOutput() {
		return output;
	}

	public void setOutput(Sink[] output) {
		this.output = output;
	}
}