/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.ssl;

import java.math.BigInteger;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chris
 * 
 */
public class SslTrustManager implements X509TrustManager {

	static Logger log = LoggerFactory.getLogger(SslTrustManager.class);

	List<CertificateHandler> listener = new ArrayList<CertificateHandler>();

	public SslTrustManager() {
	}

	public void addListener(CertificateHandler l) {
		listener.add(l);
	}

	/**
	 * @see javax.net.ssl.X509TrustManager#checkClientTrusted(java.security.cert.X509Certificate[],
	 *      java.lang.String)
	 */
	public void checkClientTrusted(X509Certificate[] chain, String authType)
			throws CertificateException {
		log.debug("Check if CLIENT chain is trusted for authType '{}'",
				authType);
		int i = 0;
		for (X509Certificate cert : chain) {
			log.debug("  chain[{}].subject:   {}", i, cert.getSubjectDN());
			log.debug("  chain[{}].issuer:    {}", i, cert.getIssuerDN());
			log.debug("  chain[{}].expiresAt: {}", i, cert.getNotAfter());
			i++;
		}

		for (CertificateHandler l : listener) {
			l.certificatesFound(chain);
		}
	}

	/**
	 * @see javax.net.ssl.X509TrustManager#checkServerTrusted(java.security.cert.X509Certificate[],
	 *      java.lang.String)
	 */
	public void checkServerTrusted(X509Certificate[] chain, String authType)
			throws CertificateException {
		log.debug("Check if SERVER chain is trusted for authType '{}'",
				authType);
		int i = 0;
		for (X509Certificate cert : chain) {
			BigInteger serial = cert.getSerialNumber();
			log.debug("  chain[{}].serial:    {}", i, serial.toString(16));
			log.debug("  chain[{}].subject:   {}", i, cert.getSubjectDN());
			log.debug("  chain[{}].issuer:    {}", i, cert.getIssuerDN());
			log.debug("  chain[{}].expiresAt: {}", i, cert.getNotAfter());
			log.debug("  chain[{}].validFrom: {}", i, cert.getNotBefore());

			Set<String> extOIDs = cert.getCriticalExtensionOIDs();
			// log.debug("      chain[{}].extOIDs:  {}", i, extOIDs);
			//
			// for (String oid : extOIDs) {
			// try {
			// byte[] ext = cert.getExtensionValue(oid);
			// log.debug("    chain[{}].{} = " + new String(ext), i, oid);
			// } catch (Exception e) {
			// e.printStackTrace();
			// }
			// }

			extOIDs = cert.getNonCriticalExtensionOIDs();
			log.debug("      chain[{}].nonCriticalExtOIDs:  {}", i, extOIDs);

			// for (String oid : extOIDs) {
			// try {
			// byte[] ext = cert.getExtensionValue(oid);
			//
			// String val = new String(ext);
			// log.debug(
			// "    chain[{}].{}  SEQUENCE?  "
			// + StringDecoder.isSequence(ext), i, oid);
			// log.debug("    chain[{}].{} = " + StringDecoder.decode(ext),
			// i, oid);
			//
			// if (oid.equals("2.5.29.31")) {
			// int idx = val.indexOf("http");
			// if (idx >= 0) {
			// val = val.substring(idx);
			// }
			// log.debug("CRL URL is: {}", val);
			// }
			//
			// } catch (Exception e) {
			// e.printStackTrace();
			// }
			// }

			i++;
		}

		for (CertificateHandler l : listener) {
			l.certificatesFound(chain);
		}
	}

	/**
	 * @see javax.net.ssl.X509TrustManager#getAcceptedIssuers()
	 */
	public X509Certificate[] getAcceptedIssuers() {
		return new X509Certificate[0];
	}
}
