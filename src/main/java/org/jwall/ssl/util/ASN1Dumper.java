/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.ssl.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Sequence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chris
 * 
 */
public class ASN1Dumper {

	static Logger log = LoggerFactory.getLogger(ASN1Dumper.class);

	public Map<String, Object> dump(ASN1Sequence seq) {
		final Map<String, Object> out = new LinkedHashMap<String, Object>();
		final List<Object> obs = new ArrayList<Object>();

		for (int i = 0; i < seq.size(); i++) {

			ASN1Encodable obj = seq.getObjectAt(i);
			log.info("Need to dump {}", obj);

			Map<String, Object> dump = dump(obj);
			obs.add(dump);
		}

		out.put("values", obs);
		return out;
	}

	public Map<String, Object> dump(ASN1Encodable obj) {
		final Map<String, Object> out = new LinkedHashMap<String, Object>();

		return out;
	}
}
