/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.ssl.util;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chris
 * 
 */
public class StringDecoder {

	static Logger log = LoggerFactory.getLogger(StringDecoder.class);

	public static List<String> decode(byte[] data) {
		final List<String> results = new ArrayList<String>();

		StringBuffer s = new StringBuffer();

		for (int i = 0; i < data.length; i++) {
			if (data[i] == '\0') {
				log.debug("Found \\0 at {}", i);
				results.add(s.toString());
				s = new StringBuffer();
			} else {
				s.append(((char) data[i]));
			}
		}

		if (s.length() > 0) {
			results.add(s.toString());
		}

		return results;
	}

	public static boolean isSequence(byte[] data) {

		byte t = data[0];
		log.debug("type: {}", bitString(t));

		return false;
	}

	public static String bitString(byte b) {
		StringBuffer s = new StringBuffer();

		int v = b;

		for (int i = 7; i >= 0; i--) {
			if ((v & (1 << i)) > 0) {
				s.append(1);
			} else {
				s.append(0);
			}
		}

		return s.toString();
	}

	public static void main(String[] args) {
		System.out.println("3 = " + bitString((byte) 3));
	}
}
