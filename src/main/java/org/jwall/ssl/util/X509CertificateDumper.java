/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.ssl.util;

import java.io.PrintStream;
import java.net.URL;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bouncycastle.util.encoders.Base64;

/**
 * @author chris
 * 
 */
public class X509CertificateDumper {

	public final static String[] KEY_USAGE_FLAGS = new String[] {
			"digitalSignature", "nonRepudiation", "keyEncipherment",
			"dataEncipherment", "keyAgreement", "keyCertSign", "cRLSign",
			"encipherOnly", "decipherOnly" };

	public Map<String, Object> dump(X509Certificate cert) {
		final Map<String, Object> out = new LinkedHashMap<String, Object>();

		out.put("serial", "0x" + cert.getSerialNumber().toString(16));
		out.put("subject", cert.getSubjectDN().toString());
		out.put("issuer", cert.getIssuerDN());
		out.put("valid.notBefore", cert.getNotBefore().getTime());
		out.put("valid.notAfter", cert.getNotAfter().getTime());
		out.put("signature.algorithm.name", cert.getSigAlgName());
		out.put("signature.algorithm.oid", cert.getSigAlgOID());

		out.put("keyUsage", dumpFlags(KEY_USAGE_FLAGS, cert.getKeyUsage()));
		out.put("publicKey",
				new String(Base64.encode(cert.getPublicKey().getEncoded())));

		final List<Object> exts = new ArrayList<Object>();

		Set<String> extOIDs = cert.getNonCriticalExtensionOIDs();
		for (String oid : extOIDs) {
			byte[] derData = cert.getExtensionValue(oid);
			Map<String, Object> extDump = new HashMap<String, Object>();
			extDump.put(oid, new String(Base64.encode(derData)));
			exts.add(extDump);
		}
		out.put("extensions", exts);
		return out;
	}

	private List<String> dumpFlags(String[] flagNames, boolean[] vals) {
		List<String> out = new ArrayList<String>();
		for (int i = 0; i < flagNames.length && i < vals.length; i++) {
			if (vals[i]) {
				out.add(flagNames[i]);
			}
		}
		return out;
	}

	@SuppressWarnings("unchecked")
	public static void dump(int depth, Map<String, Object> map, PrintStream p) {
		String pre = "";
		for (int i = 0; i < depth; i++) {
			pre += "  ";
		}
		String level = pre;
		pre += "  ";

		p.println(level + "{");
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			p.print(pre + "'" + key + "': ");

			Object value = map.get(key);
			if (value.toString().matches("(0x)?[0-9a-f]*")) {
				p.print(value.toString());
			} else {

				if (value instanceof List) {
					List<Object> ls = (List<Object>) value;
					p.print("[");
					Iterator<Object> li = ls.iterator();
					while (li.hasNext()) {
						Object lo = li.next();
						if (lo instanceof String) {
							p.print("'" + lo + "'");
						} else
							p.print(lo);

						if (li.hasNext())
							p.print(", ");
					}
					p.print("]");
				} else {
					p.print("'" + value + "'");
				}

				// if (value instanceof Collection) {
				// p.print("[");
				// Iterator<Object> cv = ((Collection) value).iterator();
				// while (it.hasNext()) {
				// Object v = it.next();
				//
				// p.print("'");
				// p.print(JSONObject.escape(v.toString()));
				// p.print("'");
				//
				// if (it.hasNext())
				// p.print(", ");
				// }
				// p.print("]");
				// } else {
				// p.print("'" + map.get(key) + "'");
				// }
			}

			if (it.hasNext())
				p.print(", ");
			p.println();
		}
		p.println(level + "}");
	}

	public static void main(String[] args) throws Exception {

		URL url = X509CertificateDumper.class
				.getResource("/secure.jwall.org.cer");
		X509Certificate cert = (X509Certificate) CertificateFactory
				.getInstance("X509").generateCertificate(url.openStream());

		Map<String, Object> out = new X509CertificateDumper().dump(cert);
		// System.out.println(out);
		dump(0, out, System.out);
	}
}
