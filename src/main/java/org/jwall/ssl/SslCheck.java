/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.ssl;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLEntry;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.net.ssl.KeyManager;
import javax.net.ssl.TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.util.URLUtilities;

/**
 * @author chris
 * 
 */
public class SslCheck {

	static Logger log = LoggerFactory.getLogger(SslCheck.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {

		final CrlCollector col = new CrlCollector();

		HttpsChecker https = new HttpsChecker();
		https.addHandler(col);

		// https.check("www.google.com", 443);
		// https.check("www.google.de", 443 );
		// https.check( "www.heise.de", 443);

		// https.check("zattoo.com", 443);
		// https.check("www.thalia.de", 443);
		https.check("secure.jwall.org", 443);

		log.info("collector has {} CRL urls collected", col.collectedUrls()
				.size());
		CertificateFactory cf = CertificateFactory.getInstance("X509");

		SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");

		for (CrlLocation url : col.collectedUrls()) {
			try {
				File file = new File("/opt/jwall/var/data/crls/" + url.id()
						+ "/" + fmt.format(new Date()) + ".crl");

				file.getParentFile().mkdirs();

				URL curl = new URL(url.url());

				URLUtilities.copy(curl, file);

				X509CRL crl = (X509CRL) cf
						.generateCRL(new FileInputStream(file));

				Date update = crl.getThisUpdate();
				Date nextUpdate = crl.getNextUpdate();

				log.info("Checking CRL at {}", url.url());
				log.info("Update time of CRL: {}", update);
				log.info("Next update of CRL: {}", nextUpdate);

				Set<? extends X509CRLEntry> revoked = crl
						.getRevokedCertificates();
				log.info("CRL contains {} revoked certificates.",
						revoked.size());

				int i = 0;
				for (X509CRLEntry entry : revoked) {

					Date revokedAt = entry.getRevocationDate();

					log.info("entry is of class {}", entry.getClass());
					log.info("   revoked[{}].serial:    {}", i, entry
							.getSerialNumber().toString(16));
					log.info("   revoked[{}].issuer:    {}", i,
							entry.getCertificateIssuer());
					log.info("   revoked[{}].revokedAt: {}", i, revokedAt);

					i++;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public class TrustMng implements TrustManager {

	}

	public class KeyMng implements KeyManager {

	}
}
