/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.ssl;

import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.x509.extension.X509ExtensionUtil;
import org.jwall.ssl.util.StringDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chris
 * 
 */
public class CrlCollector implements CertificateHandler {

	static Logger log = LoggerFactory.getLogger(CrlCollector.class);
	final Set<CrlLocation> crlUrls = new TreeSet<CrlLocation>();

	/**
	 * @see org.jwall.mp3.CertificateHandler#certificateFound(java.security.cert.X509Certificate)
	 */
	public void certificateFound(X509Certificate cert) {

		for (String oid : cert.getNonCriticalExtensionOIDs()) {
			try {
				byte[] ext = cert.getExtensionValue(oid);

				List<String> parts = StringDecoder.decode(ext);

				String val = new String(ext);
				// log.debug("    cert[{}].{} = ", oid, val);

				if (oid.equals("2.5.29.31")) {

					ASN1Primitive asn1 = X509ExtensionUtil
							.fromExtensionValue(ext);
					log.debug("Found OID {}: {}", oid, asn1);

					int hash = asn1.toString().indexOf("#");
					if (hash >= 0) {

						String str = asn1.toString().substring(hash + 1);

						int end = asn1.toString().indexOf("]", hash + 1);
						if (end > 0) {
							str = asn1.toString().substring(hash + 1, end);
						}

						String b64 = str;
						log.debug("b64 = {}", b64);
						log.debug("ascii-decorded => '{}'", dec(str));

						crlUrls.add(new CrlLocation(dec(str).trim()));
					}

					// DLSequence seq = (DLSequence) asn1;
					// for (int i = 0; i < seq.size(); i++) {
					// ASN1Encodable obj = seq.getObjectAt(i);
					// log.debug("  object[{}] = {}", i, obj);
					// DLSequence s = (DLSequence) obj;
					// for (int k = 0; k < s.size(); k++) {
					// ASN1Encodable o = s.getObjectAt(k);
					// log.debug("   k[{}] = {}", k, o);
					// }
					// }
					// log.debug("parts: {}", parts);
					//
					// int idx = val.indexOf("http");
					// if (idx >= 0) {
					// val = val.substring(idx);
					// }
					// log.debug("CRL URL is: {}", val);
					// crlUrls.add(new CrlLocation(val.trim()));
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @see org.jwall.mp3.CertificateHandler#certificatesFound(java.security
	 *      .cert.X509Certificate[])
	 */
	public void certificatesFound(X509Certificate[] chain) {
		for (X509Certificate cert : chain) {
			certificateFound(cert);
		}
	}

	public Set<CrlLocation> collectedUrls() {
		return crlUrls;
	}

	public String dec(String in) {
		StringBuffer s = new StringBuffer();

		for (int i = 0; i + 1 < in.length(); i += 2) {
			String sub = in.substring(i, i + 2);

			int c = Integer.parseInt(sub, 16);

			// log.debug(" '{}' => {}", sub, c);
			s.append(((char) c));
		}

		return s.toString();
	}
}
