/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.ssl;

import java.io.PrintStream;
import java.net.Socket;
import java.net.URL;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import stream.util.URLUtilities;

/**
 * @author chris
 * 
 */
public class HttpsChecker {

	final List<CertificateHandler> handlers = new ArrayList<CertificateHandler>();

	public void check(URL url) throws Exception {
		check(url.getHost(), url.getPort());
	}

	public void check(String host, int port) throws Exception {

		SSLContext ctx = SSLContext.getInstance("TLS");

		final KeyManagerFactory kmf = KeyManagerFactory
				.getInstance(KeyManagerFactory.getDefaultAlgorithm());

		URL ksUrl = new URL(
				"file:/Library/Java/JavaVirtualMachines/jdk1.8.0.jdk/Contents/Home/jre/lib/security/cacerts");
		KeyStore ks = KeyStore.getInstance("JKS");
		ks.load(ksUrl.openStream(), "changeit".toCharArray());
		kmf.init(ks, "changeit".toCharArray());

		SslTrustManager sslTrustManager = new SslTrustManager();

		for (CertificateHandler h : handlers) {
			sslTrustManager.addListener(h);
		}

		ctx.init(kmf.getKeyManagers(), new TrustManager[] { sslTrustManager },
				null);

		SSLSocketFactory ssf = ctx.getSocketFactory();

		Socket socket = ssf.createSocket(host, port);

		try {

			PrintStream out = new PrintStream(socket.getOutputStream());
			out.println("GET / HTTP/1.0");
			out.println("Host: " + host);
			out.println("User-Agent: Just a badly programmed robot.");
			out.println("Connection: close");
			out.println();
			out.flush();

			URLUtilities.readResponse(socket.getInputStream());
			// System.out.println("Response:\n" + res);

			out.close();
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void addHandler(CertificateHandler handler) {
		if (!handlers.contains(handler)) {
			handlers.add(handler);
		}
	}
}
