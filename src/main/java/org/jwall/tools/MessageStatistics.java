/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.Properties;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.zip.GZIPInputStream;
import java.net.URL;
import java.net.URLConnection;

import org.jwall.util.MultiSet;
import org.jwall.web.audit.AuditEventMessage;
import org.jwall.web.audit.io.MessageParser;

public class MessageStatistics implements CommandLineTool {

	public final static String PROPERTY_UPLOAD_URL="upload.url";
	public final static String PROPERTY_UPLOAD_USERNAME = "upload.username";
	public final static String PROPERTY_UPLOAD_PASSWORD = "upload.password";

	public final static String[] SYS_PROPERTIES = {
		PROPERTY_UPLOAD_URL,
		PROPERTY_UPLOAD_USERNAME,
		PROPERTY_UPLOAD_PASSWORD
	};

	boolean quiet = false;
	File outputFile = null;
	PrintStream out = new PrintStream( System.out );
	Properties p = new Properties();

	public MessageStatistics(){
		p.setProperty( PROPERTY_UPLOAD_URL, "https://secure.jwall.org/stats/upload" );
		p.setProperty( PROPERTY_UPLOAD_USERNAME, "" );
		p.setProperty( PROPERTY_UPLOAD_PASSWORD, "" );

		for( String key : SYS_PROPERTIES ){
			if( System.getenv( key ) != null ){
				p.setProperty( key, System.getenv( key ) );
			}
		}
	}

	MultiSet<String> msgStats = new MultiSet<String>();
	MultiSet<String> tagStats = new MultiSet<String>();
	MultiSet<String> ruleStats = new MultiSet<String>();

	@Override
	public String getName() {
		return "attack-statistics";
	}

	@Override
	public void run(String[] args) throws Exception {

		boolean gzip = "1".equals( System.getProperty( "use.gzip" ) );

		if( args.length == 0 ){
			System.out.println( "You need to specify at least a file to read from!" );
			System.out.println( "Use '-' to read from stdin." );
			System.exit( -1 );
		}

		InputStream in = null;
		if( args[0].trim().equals( "-" ) ){
			in = System.in;
		} else {
			File input = new File( args[0] );
			if( !input.canRead() ){
				System.err.println( "Cannot open file '" + input.getAbsolutePath() + "' for reading!" );
				System.exit( -1 );
			}
			in = new FileInputStream( input );
		}

		PrintStream out = new PrintStream( System.out );

		if( gzip ){
			System.out.println( "Assuming input to be gzip-compressed..." );
			this.process( new GZIPInputStream( in ) );
		} else 
			this.process( in );

		TreeSet<String> rmessages = new TreeSet<String>( new CountSorter(msgStats) );
		rmessages.addAll( msgStats.getValues() );
		TreeSet<String> rules = new TreeSet<String>( new CountSorter(ruleStats) );
		rules.addAll( ruleStats.getValues() );
		TreeSet<String> tags = new TreeSet<String>( new CountSorter( tagStats ) );
		tags.addAll( tagStats.getValues() );
		out.println();
		out.println( "------------------------------------------------------" );
		out.println( "Rule Messages:" );
		print( rmessages, msgStats );

		out.println();
		out.println( "------------------------------------------------------" );
		out.println( "Rule-IDs:" );
		print( rules, ruleStats );

		out.println();
		out.println( "------------------------------------------------------" );
		out.println( "Tags:" );
		print( tags, tagStats );

		out.println( "------------------------------------------------------" );
	}


	public void process( InputStream in ) throws IOException {

		long start = System.currentTimeMillis();
		BufferedReader reader = null;
		reader = new BufferedReader( new InputStreamReader( in ) );

		String line = reader.readLine();
		int count = 0;

		while( line != null ){
			try {

				AuditEventMessage msg = MessageParser.parseMessage( line );
				if( msg != null ){
					String m = msg.getRuleMsg();
					if( m != null && ! m.trim().equals( "" ) && ! m.trim().startsWith( "Inbound Anomaly Score" ) ){
						int idx = m.indexOf( "):" );
						if( idx > 0 )
							m = m.substring( idx + 3 );

						msgStats.add( m.trim() );
					}

					if( msg.getRuleId() != null ){
						ruleStats.add( msg.getRuleId().toString() );
					}

					if( msg.getRuleTags() != null )
						for( String tag : msg.getRuleTags() )
							tagStats.add( tag );

					if( count % 1000 == 0 )
						System.out.print( "." );
					count++;
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			line = reader.readLine();
		} 
		reader.close();

		if( count == 0 ){
			System.out.println( "No messages found." );
			System.exit( 0 );
		}


		long end = System.currentTimeMillis();
		out.println();
		out.println( count + " messages processed in " + ((end-start) / 1000) + " seconds.");

	}

	public String toString( TreeSet<String> keys, MultiSet<String> stats ){
		StringBuffer s = new StringBuffer();
		for( String str : keys ){
			s.append( toString( stats.getCount( str ), str ) );
		}
		return s.toString();
	}


	public String toString( Long count, String msg ){
		StringBuffer s = new StringBuffer();
		DecimalFormat fmt = new DecimalFormat( "0" );
		String f = fmt.format( count );
		for( int i = 0; i < 10 - f.length(); i++ ){
			s.append( " " );
		}
		s.append( f + "   " +  msg );
		return s.toString();
	}


	public void print( TreeSet<String> keys, MultiSet<String> stats ){
		for( String str : keys ){
			print( stats.getCount( str ), str );
		}
	}

	public void print( Long count, String msg ){
		DecimalFormat fmt = new DecimalFormat( "0" );
		String f = fmt.format( count );
		for( int i = 0; i < 10 - f.length(); i++ ){
			print( " " );
		}
		println( f + "   " +  msg );
	}

	public void print( String str ){
		out.print( str );
	}

	public void println( String str ){
		out.println( str );
	}

	public void println(){
		out.println();
	}

	public class CountSorter implements Comparator<String> {
		MultiSet<String> cnts;
		public CountSorter( MultiSet<String> counts ){
			cnts = counts;
		}


		@Override
		public int compare(String o1, String o2) {

			if( o1 == o2 )
				return 0;

			int cmp = cnts.getCount( o1 ).compareTo( cnts.getCount( o2 ) );
			if( cmp == 0 )
				return o1.compareTo( o2 );

			return - cmp;
		}
	}
	public MultiSet<String> getMsgStats() {
		return msgStats;
	}
	public MultiSet<String> getTagStats() {
		return tagStats;
	}
	public MultiSet<String> getRuleStats() {
		if( ruleStats.getCount( "__TIME__" ) == 0 )
			ruleStats.addCount( "__TIME__", System.currentTimeMillis() );
		return ruleStats;
	}
	public void sendDataSet( String str, String dest ) throws Exception {
		URL url = new URL(dest);
		URLConnection con = url.openConnection();
		con.setDoOutput(true);

		PrintStream out = new PrintStream( con.getOutputStream() );
		out.print( str );
		out.flush();

		BufferedReader r = new BufferedReader( new InputStreamReader( con.getInputStream() ) );
		String line = r.readLine();
		while( line != null ){
			System.err.println( "Response: " + line );
			line = r.readLine();
		}
		r.close();
		out.close();
	}
}
