/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.File;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.jwall.Collector;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.jwall.web.audit.net.AuditEventConsoleSender;

public class EventDirectorySender
implements CommandLineTool
{
    static Logger log = Logger.getLogger( EventDirectorySender.class.getName() );

    long minAge = 0L;
    static boolean deleteFiles = false;
    static boolean quietMode = false;
    
    public String getName()
    {
        return "EventDirectorySender";
    }

    public void run(String[] args)
    throws Exception
    {
        try {

            LogManager lm = LogManager.getLogManager();

            URL url = EventDirectorySender.class.getResource( "/logging.properties" );

            if( url != null ){
                lm.readConfiguration( url.openStream() );
                log.info( "Configuring logging from " + url.toString() );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        LinkedList<String> params = new LinkedList<String>();
        for( String arg : args )
            params.add( arg );


        try {
            minAge = Long.parseLong( System.getProperty( "minAge" ) );
        } catch (Exception e){
            minAge = 0L;
        }

        int delay = 0;

        try {
            delay = Integer.parseInt( System.getProperty( "delay" ) );
        } catch (Exception e) {
            delay = 0;
        }
        
        
        try {
            deleteFiles = Boolean.parseBoolean( System.getProperty( "deleteFiles" ) );
        } catch (Exception e) {
            deleteFiles = false;
        }
        
        try {
            quietMode = Boolean.parseBoolean( System.getProperty( "quietMode" ) );
        } catch (Exception e) {
            quietMode = true;
        }


        if( params.size() != 2 ){
            System.out.println();
            System.out.println();
            System.out.println();
            System.out.println( "  org.jwall.tools.EventDirectorySender" );
            System.out.println( "  ------------------------------------\n" );
            System.out.println( "  This helpful tool allows for sending events to a console ( AuditConsole or ModSecurity" );
            System.out.println( "  console). In that respect it acts as a command-line version of the mlogc tool provided");
            System.out.println( "  with ModSecurity itself." );
            System.out.println();
            System.out.println( "  Usage:" );
            System.out.println( "  \tjava -jar " + Tools.TOOL_JAR + "  send-dir  CONSOLE_URL  AUDIT_LOG_DATA_DIR\n" );
            System.out.println( "  Where the CONSOLE_URL is the URL of your AuditConsole receiver, i.e. the same which" );
            System.out.println( "  you use for sending events to the console with mlgoc." );
            System.out.println();
            System.out.println( "  This URL also needs to include the sensor-name and the password for the sensor." );
            System.out.println( "  AUDIT_LOG_DATA_DIR sipmly is the name (and path to) of the directory where your");
            System.out.println( "  events reside. This directory is recursively scanned for ModSecurity2 audit-events." );
            System.out.println();
            System.out.println( "  Example:" );
            System.out.println( "  \tjava -jar " + Tools.TOOL_JAR + "  send  http://sensor:test@localhost:8080/rpc/auditLogReceiver  /var/log/mlogc/data" );
            System.out.println();
            System.out.println( "  This will send all events contained in the  files within \"/var/log/mlogc/data\" to the console running" );
            System.out.println( "  on the localhost at port 8080. The sender will identify itself as the sensor \"sensor\"" );
            System.out.println( "  using the password \"test\"." );
            System.out.println();
            System.out.println();
            return;
        }

        URL url = new URL( params.removeFirst() );

        String host = url.getHost();
        int port = url.getPort();

        String user = "";
        String pass = "";
        String userinfo = url.getUserInfo();

        if( userinfo != null && userinfo.indexOf( ":" ) >= 0 ){
            String[] tok =userinfo.split(":");
            user = tok[0];
            pass = tok[1];
        }

        log.finest("UserInfo = " + url.getUserInfo() );

        Collector.p.setProperty( AuditEventConsoleSender.CONSOLE_CONNECTION_SSL, url.getProtocol().equalsIgnoreCase( "https" ) + "" );
        Collector.p.setProperty( AuditEventConsoleSender.CONSOLE_CONNECTION_KEEP_ALIVE, "true" );

        log.info( "Setting CONSOLE_URI to '" + url.getPath() + "'");
        Collector.p.setProperty( AuditEventConsoleSender.CONSOLE_URI, url.getPath() );


        AuditEventConsoleSender sender = new AuditEventConsoleSender( host, port, user, pass );

        if( params.isEmpty() ){
            System.err.println( "You need to specify a data-directory!" );
            System.exit( -1 );
        }

        File directory = new File( params.removeFirst() );
        if( ! directory.isDirectory() ){
            System.err.println( "The specified directory does not exist or is not a directory!" );
            System.exit( -1 );
        }

        System.out.println( "+----------------------------------------------------------------------------" );
        System.out.println( "| " );
        System.out.println( "|  Collecting list of event-files found in '" + directory.getAbsolutePath() + "'" );
        List<File> files = findAuditEventFiles( directory, minAge );
        System.out.println( "| " );
        System.out.println( "|  Found " + files.size() + " files in ModSecurity2 AuditLog format." );
        System.out.println( "| " );
        
        if( ! quietMode ){
            System.out.println( "|  Please press RETURN to start sending the events  or  press CTRL+C to abort..." );
            System.in.read();
        }

        Integer events = 0;
        Integer count = 0;
        Integer total = files.size();

        for( File file : files ){

            //log.info( "Sending event(s) from file '" + file.getAbsolutePath() + "'" );
            if( count % 100 == 0 ){
                DecimalFormat fmt = new DecimalFormat( "0.00" );
                Double compl = 100.0d * (count.doubleValue() / total.doubleValue());
                System.out.print( "\n|  Sent " + fmt.format( compl ) + "%  (" + events + " events) of the files" );
            }

            try {
                AuditEventReader eventReader = new ModSecurity2AuditReader( file, false );

                AuditEvent evt = eventReader.readNext();
                while( evt != null ){
                    //log.info( "Sending event '" + evt.getEventId() + "'" );
                    sender.sendAuditEvent( evt );
                    events++;
                    if( count % 10 == 0 )
                        System.out.print( "." );
                    evt = eventReader.readNext();

                    if( delay > 0 ){
                        try {
                            Thread.sleep( delay );
                        } catch (Exception e) {}
                    }

                }
                
                if( deleteFiles ){
                    file.delete();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            count++;
        }
        System.out.println( "|  All files sent." );
        System.out.println( "+----------------------------------------------------------------------------" );
    }


    public long memoryUsed(){
        return ( Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory() ) / ( 1024 * 1024 );
    }




    public static List<File> findAuditEventFiles( File dir, Long minAge ){

        log.fine( "Checking directory '" + dir.getAbsolutePath() + "'" );
        List<File> files = new LinkedList<File>();
        if( dir == null || dir.listFiles() == null )
        	return files;
        
        for( File file : dir.listFiles() ){

            if( file.isDirectory() ){
                files.addAll( findAuditEventFiles( file, minAge ) );
            } else {

                if( minAge < 0 || System.currentTimeMillis() - file.lastModified() >= minAge ){

                    log.fine( "Checking file " + file.getAbsolutePath() );

                    try {
                        if( AuditFormat.MOD_SECURITY_2_X_SERIAL_LOG == AuditFormat.guessFormat( file ) ){
                            log.fine( "Found event in file '" + file.getAbsolutePath() + "'" );
                            files.add( file );
                        }
                    } catch (Exception e) {
                        log.warning( "Failed to determine format of file '" + file.getAbsolutePath() + "': " + e.getMessage() );
                    }
                }
            }
        }

        log.fine( "   -> found " + files.size() + " event-files." );
        return files;
    }


    /**
     * @param args
     */
    public static void main(String[] args)
    throws Exception
    {
        EventDirectorySender sender = new EventDirectorySender();
        sender.run( args );
    }
}
