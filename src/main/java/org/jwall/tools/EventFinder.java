/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.File;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.filter.FilterCompiler;
import org.jwall.web.audit.filter.FilterExpression;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;

public class EventFinder implements CommandLineTool {

	FilterExpression expression;

	@Override
	public String getName() {
		return "find";
	}

	public FilterExpression compileFilter(String filter) throws Exception {
		expression = FilterCompiler.parse(filter);
		return expression;
	}

	public boolean matches(AuditEvent event) {
		return event != null && expression != null && expression.matches(event);
	}

	@Override
	public void run(String[] args) throws Exception {

		File file = new File(args[0]);

		AuditEventReader reader = new ModSecurity2AuditReader(file);

		compileFilter(args[1]);

		AuditEvent evt = reader.readNext();
		while (evt != null) {

			if (matches(evt)) {
				System.out.println(evt.get(ModSecurity.TX_ID));
			}

			evt = reader.readNext();
		}

	}
}
