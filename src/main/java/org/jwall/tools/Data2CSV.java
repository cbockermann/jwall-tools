/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;
import java.text.DecimalFormat;

public class Data2CSV
{

    /**
     * @param args
     */
    public static void main(String[] args)
        throws Exception
    {

        PrintStream out = new PrintStream( new FileOutputStream( new File( "/Users/chris/WetterKW39.csv" ) ) );
        BufferedReader r = new BufferedReader( new FileReader( new File( "/Users/chris/WetterKW39.dat" ) ) );
        
        DecimalFormat fmt = new DecimalFormat( "0.000000" );
        String line = r.readLine();
        while( line != null ){
            
            StringBuffer b = new StringBuffer();
            String[] tok = line.trim().split( "\\s+" );
            for( int i = 0; i < tok.length && i < 3; i++ ){
                b.append( tok[i].trim().replaceAll( ",", "." ) );
                if( i == 2 ){
                    try {
                        Double d = new Double( tok[i].trim().replace( ',', '.') );
                        b.append( ",\"" + fmt.format(d) + "\"");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                b.append( "," );
                if( i == tok.length || i + 1 == 3 )
                    b.append( "\n" );
            }
            System.out.print( b.toString() );
            out.print( b.toString() );
            
            line = r.readLine();
        }
        r.close();
    }
}
