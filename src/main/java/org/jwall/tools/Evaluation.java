/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.Vector;

import org.jwall.util.MultiSet;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;
import org.jwall.web.audit.io.ConcurrentAuditReader;
import org.jwall.web.eval.ResultLogger;
import org.jwall.web.eval.TestClient;
import org.jwall.web.eval.XMLResultLogger;
import org.jwall.web.eval.transform.AddHeaderField;
import org.jwall.web.eval.transform.DestinationAddressTransformation;
import org.jwall.web.eval.transform.HeaderFieldSubstitution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * This is the main-class for the TestClient application. It extends the
 * <i>real</i> TestClient class from package <code>org.jwall.core.eval</code>
 * and provides a main-method for initial execution.
 * 
 * The main-method starts parsing the command-line switches and creates a new
 * instance of this class to inject the events of the audit-log file given via
 * the command-line.
 * 
 * @author Christian Bockermann <chris@jwall.org>
 * @version $Id: TestClient.java 11 2007-08-31 09:23:51Z chris $
 * 
 */
public class Evaluation implements CommandLineTool {

	public final static String PROPERTY_LIMIT = "limit";
	public final static String PROPERTY_FAKE_SESSIONS = "fake-sessions";
	public final static String PROPERTY_RANGE_FROM = "from";
	public final static String PROPERTY_RANGE_TO = "to";
	public final static String PROPERTY_DESTINATION = "destination";
	public final static String PROPERTY_DESTINATION_PORT = "org.jwall.web.eval.server.port";
	public final static String PROPERTY_USER_AGENT = "user-agent";
	public final static String PROPERTY_EVALUATION_ID = "evaluation-id";

	/** A single logger for this class */
	static Logger log = LoggerFactory.getLogger(Evaluation.class);

	public final static String DESCRIPTION = "";

	public static Properties parseArguments(String[] args) throws Exception {
		Properties p = new Properties();

		for (int i = 0; i < args.length - 1; i++) {
			if ("-l".equals(args[i]) || "--limit".equals(args[i])) {
				String s = args[i + 1];
				try {
					Long limit = Long.parseLong(s);
					p.setProperty(PROPERTY_LIMIT, limit.toString());
				} catch (Exception e) {
					throw new Exception(
							"Parameter limit needs an integer argument!");
				}
			}

			if ("--fake-session".equals(args[i])) {
				System.out
						.println("Creating sessionIDs from RemoteAddress/User-Agent");
				// cl.createFakeSessions( true );
				p.setProperty(PROPERTY_FAKE_SESSIONS, "true");
			}

			if ("-r".equals(args[i]) || "--range".equals(args[i])) {

				if (!args[i + 1].matches("\\d+:\\d+")) {
					System.out
							.println("Specification of the range has to be in the from   start:end !");
					System.exit(-1);
				} else {
					String[] tok = args[i + 1].split(":");

					Long rangeFrom = Long.parseLong(tok[0]);
					p.setProperty(PROPERTY_RANGE_FROM, rangeFrom.toString());
					Long rangeTo = Long.parseLong(tok[1]);
					p.setProperty(PROPERTY_RANGE_TO, rangeTo.toString());
					// Long limit = rangeTo - rangeFrom;

				}
			}

			if ("-d".equals(args[i]) || "--destination".equals(args[i])) {
				try {

					int idx = args[i + 1].indexOf(":");

					if (idx > 0) {

						InetAddress addr = InetAddress.getByName(args[i + 1]
								.substring(0, idx));
						p.setProperty(Evaluation.PROPERTY_DESTINATION,
								addr.getHostAddress());
						p.setProperty(Evaluation.PROPERTY_DESTINATION_PORT,
								args[i + 1].substring(idx + 1));
						// server = addr.getHostAddress();
						// cl.getTransformations().add( new
						// DestinationAddressTransformation( args[ i + 1 ] ) );

					} else {

						InetAddress addr = InetAddress.getByName(args[i + 1]);
						// server = addr.getHostAddress();
						p.setProperty(Evaluation.PROPERTY_DESTINATION,
								addr.getHostAddress());
						// cl.getTransformations().add( new
						// DestinationAddressTransformation( args[ i + 1 ] ) );

					}

				} catch (Exception e) {
					e.printStackTrace();
					System.err
							.println("Invalid destination address specified!");
					System.out.println();
					System.exit(-1);
				}
			}

			if ("--user-agent".equals(args[i])) {

				if (i + 1 < args.length)
					p.setProperty(Evaluation.PROPERTY_USER_AGENT, args[i + 1]);
				else
					throw new Exception(
							"--user-agent option needs an user-agent string as parameter!");
			}
		}

		return p;
	}

	public String getName() {
		return "evaluation";
	}

	public void run(String[] args) throws Exception {

		log.info("jwall-tools/Evaluation - " + Tools.VERSION);

		// System.out.print("Started with arguments: ");
		// for (String arg : args) {
		// System.out.println(arg + " ");
		// }
		// System.out.println();

		try {
			Properties p = new Properties();
			p.putAll(System.getProperties());
			File f = new File("evaluation.properties");
			if (f.exists() && f.canRead()) {
				log.info("Reading properties from " + f.getAbsolutePath());
				p.load(new FileInputStream(f));
			} else {
				log.info("Property file " + f.getAbsolutePath() + " not found.");
			}

			List<String> remainder = Tools.handleArguments(args, p);
			if (remainder == null) {
				return;
			}

			// command-line options override config-file options
			//
			Properties user = Evaluation.parseArguments(remainder
					.toArray(new String[remainder.size()]));
			for (Object o : user.keySet()) {
				p.put(o, user.get(o));
				log.debug("Setting   '{}' = '{}'", o, user.get(o));
			}

			String evalID = UUID.randomUUID().toString().toUpperCase(); // Long.toHexString(System.currentTimeMillis());
			if (p.getProperty(Evaluation.PROPERTY_EVALUATION_ID) != null)
				evalID = p.getProperty(Evaluation.PROPERTY_EVALUATION_ID);

			log.info("Using evaluation key = " + evalID);
			ResultLogger rlog = XMLResultLogger.getInstance(evalID);

			long rangeFrom = 0;
			long limit = 0;

			long rangeTo = Long.MAX_VALUE;

			TestClient cl = new TestClient(rlog);

			if (p.getProperty(Evaluation.PROPERTY_RANGE_FROM) != null)
				rangeFrom = Long.parseLong(p
						.getProperty(Evaluation.PROPERTY_RANGE_FROM));

			if (p.getProperty(Evaluation.PROPERTY_RANGE_TO) != null) {
				rangeTo = Long.parseLong(p
						.getProperty(Evaluation.PROPERTY_RANGE_TO));
				limit = rangeTo - rangeFrom;
			}

			if (p.getProperty(Evaluation.PROPERTY_LIMIT) != null) {
				limit = Long
						.parseLong(p.getProperty(Evaluation.PROPERTY_LIMIT));
			}

			if ("true".equalsIgnoreCase(p
					.getProperty(Evaluation.PROPERTY_FAKE_SESSIONS))) {
				log.info("Trying to emulate tcp sessions...");
				cl.createFakeSessions("true".equalsIgnoreCase(p
						.getProperty(Evaluation.PROPERTY_FAKE_SESSIONS)));
			}

			if (p.getProperty(Evaluation.PROPERTY_USER_AGENT) != null)
				cl.getTransformations().add(
						new HeaderFieldSubstitution("User-Agent", ".*", p
								.getProperty(Evaluation.PROPERTY_USER_AGENT)));

			if (p.getProperty("add-evaluation-header") != null) {
				cl.getTransformations().add(
						new AddHeaderField("X-Evaluation-ID", evalID));
			}

			// else
			// cl.getTransformations().add(
			// new HeaderFieldSubstitution("User-Agent", ".*",
			// "jwall.org/Test-Client"));

			String server = p.getProperty(Evaluation.PROPERTY_DESTINATION);
			if (server == null)
				throw new Exception(
						"You need to specify the server to which the requests are to be injected!");

			Integer port = null;
			try {
				port = new Integer(
						p.getProperty(Evaluation.PROPERTY_DESTINATION_PORT));
			} catch (Exception e) {
				port = null;
			}

			cl.getTransformations().add(
					new DestinationAddressTransformation(server, port));

			File input = new File(args[args.length - 1]);
			log.info("Injecting requests from " + input.getAbsolutePath()
					+ " to server " + server);

			AuditEventReader r = null;
			if (p.getProperty("data-dir") != null) {
				log.info("Option 'data-dir' specified, reading concurrent audit-event-format...");
				File dataDir = new File(p.getProperty("data-dir"));
				r = new ConcurrentAuditReader(dataDir, input, false);

			} else {
				r = AuditFormat.createReader(input.getAbsolutePath(), false);
				if (r == null)
					throw new Exception(
							"Unable to determine the appopriate input-reader! ");
			}

			Collection<AuditEvent> evts = new Vector<AuditEvent>();
			int chunks = 0;
			Integer events = 0;

			//
			// skip the first "rangeFrom" events
			//
			long c = 0;
			boolean goOn = true;
			while (c < rangeFrom && goOn) {
				goOn = r.readNext() != null;
				c++;
			}
			log.info("Skipped " + c + " events.");

			long start = System.currentTimeMillis();
			Long test = start;
			log.info("Injecting: ");
			AuditEvent evt = null;
			Integer intervalCount = 0;
			do {

				evt = r.readNext();
				if (evt != null) {
					evts.add(evt);
					events++;

					if (evts.size() >= 1 || events == limit) {
						cl.addEvents(evts);
						cl.run();
						intervalCount += evts.size();
						chunks++;
						evts.clear();
					}

					if (events > 0 && events % 100 == 0) {
						Double dur = (System.currentTimeMillis() - test) / 1000.0d;
						log.info(events
								+ " events injected ({} requests/second)",
								(intervalCount.doubleValue() / dur
										.doubleValue()));
						test = System.currentTimeMillis();
						intervalCount = 0;
					}
				} else
					log.info("no event read!?");
			} while (evt != null && (limit == 0 || events < limit));

			if (evts != null && evts.size() > 0) {
				log.info("Sending remaining {} events", evts.size());
				cl.addEvents(evts);
				cl.run();
				intervalCount += evts.size();
				chunks++;
				evts.clear();
			}

			double t = System.currentTimeMillis() - start;

			log.debug("{} chunks sent.", chunks);
			log.info(" done");
			double d = events;

			if ((t / 1000) > 0)
				d = events / (t / 1000);
			else
				log.info("error while calculating stats ( t/1000 = "
						+ (t / 1000));

			System.out.println();
			System.out
					.println("+------------Evaluation Results------------------------------");
			System.out.println("|");
			System.out.println("|    Performance Summary");
			System.out.println("|    -------------------");
			DecimalFormat fmt = new DecimalFormat("0.00");

			System.out.println("|    (1)  Requests sent: " + events);
			System.out.println("|    (2)  Response rate: " + fmt.format(d)
					+ " requests/sec");
			System.out.println("|");

			printTable(System.out, "Response Status Summary", cl.getStati());
			printTable(System.out, "Redirect's Summary", cl.getRedirects());

			System.out
					.println("+------------Evaluation Results------------------------------");
			System.out.println();
			cl.closeAll();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void printTable(PrintStream p, String title, MultiSet<String> data) {
		p.println("|==========================================  ");
		p.println("|   ");
		p.println("|   " + title);
		p.print("|   ");
		print(p, "-", title.length());
		p.println();
		p.println("|   ");

		int max = "Value".length();
		for (String key : data.getValues()) {
			max = Math.max(max, key.length());
		}

		p.println("|    " + String.format("%" + max + "s", "Value")
				+ " | Count");
		p.print("|    ");
		print(p, "-", max);
		p.println("-|-----------------");

		for (String key : data.getValues()) {
			p.println("|    " + String.format("%" + max + "s", key) + " | "
					+ data.getCount(key));
		}
		p.println("|");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		Evaluation eval = new Evaluation();
		eval.run(args);
	}

	public static void print(PrintStream p, String s, int times) {
		for (int i = 0; i < times; i++) {
			p.print(s);
		}
	}
}
