/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

import org.jwall.util.MacroExpander;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.filter.AuditEventFilter;
import org.jwall.web.audit.filter.FilterCompiler;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;

public class AuditLogConverter implements CommandLineTool {

	@Override
	public String getName() {
		return "convert";
	}

	@Override
	public void run(String[] args) throws Exception {

		AuditEventFilter filter = null;

		if (System.getProperty("filter") != null) {
			filter = FilterCompiler.compile(System.getProperty("filter"));
		}

		if (args.length < 2) {
			System.out.println("Missing parameters!");
			System.out.println();
			Tools.print(AuditLogConverter.class
					.getResource("/help/AuditLogConverter.txt"));
			System.exit(-1);
		}

		long skip = 0L;
		long limit = Long.MAX_VALUE;
		long count = 0L;

		try {
			limit = new Long(System.getProperty("events.limit"));
		} catch (Exception e) {
			limit = Long.MAX_VALUE;
		}

		String pattern = args[0];
		File file = new File(args[1]);

		PrintStream out = System.out;
		if (args.length > 2)
			out = new PrintStream(new FileOutputStream(new File(args[2])));

		MacroExpander expander = new MacroExpander();

		AuditEventReader reader = AuditFormat.createReader(
				file.getAbsolutePath(), false);
		AuditEvent evt = reader.readNext();

		while (count < skip) {
			evt = reader.readNext();
			count++;
		}
		count = 0L;
		long start = System.currentTimeMillis();
		while (evt != null && count < limit) {
			if (filter == null || filter.matches(evt)) {
				String line = expander.expand(pattern, evt);
				out.println(line);
				count++;
			}
			evt = reader.readNext();
		}
		long end = System.currentTimeMillis();
		out.close();
		System.err.println(count + " events processed in "
				+ ((end - start) / 1000) + " seconds.");
	}
}