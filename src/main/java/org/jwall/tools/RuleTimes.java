/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.File;
import java.net.URL;
import java.util.Map;

import org.jwall.web.eval.RuleTimeStatistics;
import org.jwall.web.eval.RuleTimer;

import stream.Data;
import stream.ProcessorList;
import stream.data.Statistics;
import stream.io.LineStream;
import stream.io.SourceURL;
import stream.util.URLUtilities;

public class RuleTimes implements CommandLineTool {

	@Override
	public String getName() {
		return "rule-times";
	}

	@Override
	public void run(String[] args) throws Exception {

		File file = new File(args[0]);

		LineStream stream = new LineStream(new SourceURL(file.toURI().toURL()));
		stream.setFormat("[%(DATETIME)] [%(REQID)][%(TXID)][%(URI)][%(LOGLEVEL)] %(message)");
		stream.init();

		RuleTimer timer = new RuleTimer();
		RuleTimeStatistics stats = new RuleTimeStatistics();

		ProcessorList list = new ProcessorList();
		list.getProcessors().add(timer);
		list.getProcessors().add(stats);

		Data item = stream.readNext();
		int cnt = 10000000;
		while (item != null && cnt-- > 0) {
			list.process(item);
			item = stream.readNext();
		}

		URL url = RuleTimes.class.getResource("/rule-times-info.txt");
		System.out.println(URLUtilities.readContentOrEmpty(url));

		Map<String, Statistics> aggregates = stats.getStatistics();

		System.out
				.println("   +-------------+-------------+--------------+----------------+------------+");
		System.out.printf("   |%12s | %10s |  %10s  | %10s |%10s  |",
				"Rule Id", "Invocations", "total time", "transformation",
				"operator");
		System.out.println();
		System.out
				.println("   +-------------+-------------+--------------+----------------+------------+");

		String fmt = "   |%12s |   %8d  |    %8.3f  |     %8.3f   |   %8.3f |";

		for (String group : aggregates.keySet()) {
			Statistics st = aggregates.get(group);
			Double d = st.get("rule:invocations");
			st = st.divideBy(d);
			st.put("rule:invocations", d);

			System.out.printf(fmt, group, d.intValue(), st.get("rule:time"),
					st.get("rule:transformationTime"),
					st.get("rule:operatorTime"));
			System.out.println();
		}
		System.out
				.println("   +-------------+-------------+--------------+----------------+------------+\n\n");
	}

	public static void main(String args[]) throws Exception {
		RuleTimes rt = new RuleTimes();

		if (args.length > 0)
			rt.run(args);
		else
			rt.run(new String[] { "/www/proxy.jwall.org-debug.log" });
	}
}
