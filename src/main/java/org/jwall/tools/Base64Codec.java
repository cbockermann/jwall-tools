/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Base64Codec
    implements CommandLineTool
{

    @Override
    public String getName()
    {
        return "b64codec";
    }

    @Override
    public void run(String[] args) throws Exception
    {
        if( args.length <= 0){
            System.err.println( "Missing argument! You need to specify a file to decode!" );
        }
        
        File file = new File( args[0] );
        BufferedReader r = new BufferedReader( new FileReader( file ) );
        StringBuffer buf = new StringBuffer();
        String line = r.readLine();
        while( line != null ){
            buf.append( line );
            buf.append( "\n" );
            line = r.readLine();
        }
        r.close();
        org.jwall.web.audit.util.Base64Codec b64 = new org.jwall.web.audit.util.Base64Codec();
        byte[] decoded = b64.decode( buf.toString().getBytes() );
        System.out.println( new String( decoded ) );
    }
}
