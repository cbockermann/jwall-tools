/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.File;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.jwall.Collector;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ConcurrentAuditReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.jwall.web.audit.io.ParseException;
import org.jwall.web.audit.net.AuditEventConsoleSender;

public class EventSender
implements CommandLineTool
{
    static Logger log = Logger.getLogger( EventSender.class.getName() );
    static Double randDelay = 0.0d;


    public String getName()
    {
        return "EventSender";
    }

    public void run(String[] args)
        throws Exception
    {
        try {

            LogManager lm = LogManager.getLogManager();

            URL url = EventSender.class.getResource( "/logging.properties" );

            if( url != null ){
                lm.readConfiguration( url.openStream() );
                log.info( "Configuring logging from " + url.toString() );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        LinkedList<String> params = new LinkedList<String>();
        for( String arg : args )
            params.add( arg );

        int limit = Integer.MAX_VALUE;

        Iterator<String> it = params.iterator();
        while( it.hasNext() ){

            String p = it.next();
            if( p.startsWith( "--limit" ) ){
                it.remove();
                limit = Integer.parseInt( it.next() );
                it.remove();
            }
        }

        if( params.size() != 2 ){
            System.out.println();
            System.out.println();
            System.out.println();
            System.out.println( "  org.jwall.tools.EventSender" );
            System.out.println( "  ---------------------------\n" );
            System.out.println( "  This helpful tool allows for sending events to a console ( AuditConsole or ModSecurity" );
            System.out.println( "  console). In that respect it acts as a command-line version of the mlogc tool provided");
            System.out.println( "  with ModSecurity itself." );
            System.out.println();
            System.out.println( "  Usage:" );
            System.out.println( "  \tjava -jar " + Tools.TOOL_JAR + "  send  CONSOLE_URL  AUDIT_LOG_FILE\n" );
            System.out.println( "  Where the CONSOLE_URL is the URL of your AuditConsole receiver, i.e. the same which" );
            System.out.println( "  you use for sending events to the console with mlgoc." );
            System.out.println();
            System.out.println( "  This URL also needs to include the sensor-name and the password for the sensor." );
            System.out.println( "  AUDIT_LOG_FILE sipmly is the name (and path to) of the file you want the sender-tool");
            System.out.println( "  to read events from." );
            System.out.println();
            System.out.println( "  Example:" );
            System.out.println( "  \tjava -jar " + Tools.TOOL_JAR + "  send  http://sensor:test@localhost:8080/rpc/auditLogReceiver  logfile.log" );
            System.out.println();
            System.out.println( "  This will send all events contained in the  file \"logfile.log\" to the console running" );
            System.out.println( "  on the localhost at port 8080. The sender will identify itself as the sensor \"sensor\"" );
            System.out.println( "  using the password \"test\"." );
            System.out.println();
            System.out.println();
            return;
        }

        try {
            randDelay = Double.parseDouble( System.getProperty( "random.delay" ) );
        } catch (Exception e) {
            randDelay = 0.0d;
        }

        int skip = 0;
        try {
            if( System.getProperty( "events.skip" ) != null )
                skip = Integer.parseInt( System.getProperty("events.skip") );
        } catch (Exception e) {
            skip = 0;
            log.severe( "Invalid value '" + System.getProperty( "events.skip" ) + "' for parameter 'events.skip':" + e.getMessage() );
            System.exit( -1 );
        }

        int l = limit;
        try {
            if( System.getProperty( "events.limit" ) != null ){
                l = Integer.parseInt( System.getProperty( "events.limit" ) );
                limit = l;
            }
        } catch (Exception e) {
            log.severe( "Invalid value for parameter 'events.limit': " + e.getMessage() );
            System.exit( -1 );
        }


        URL url = new URL( params.removeFirst() );

        String host = url.getHost();
        int port = url.getPort();

        String user = "";
        String pass = "";
        String userinfo = url.getUserInfo();

        if( userinfo != null && userinfo.indexOf( ":" ) >= 0 ){
            String[] tok =userinfo.split(":");
            user = tok[0];
            pass = tok[1];
        }

        log.finest("UserInfo = " + url.getUserInfo() );

        Collector.p.setProperty( AuditEventConsoleSender.CONSOLE_CONNECTION_SSL, url.getProtocol().equalsIgnoreCase( "https" ) + "" );
        Collector.p.setProperty( AuditEventConsoleSender.CONSOLE_CONNECTION_KEEP_ALIVE, "true" );

        log.info( "Setting CONSOLE_URI to '" + url.getPath() + "'");
        Collector.p.setProperty( AuditEventConsoleSender.CONSOLE_URI, url.getPath() );


        AuditEventConsoleSender sender = new AuditEventConsoleSender( host, port, user, pass );

        AuditEventReader reader = null;
        
        if( params.size() > 1 ){
            
            File index = new File( params.removeFirst() );
            if( ! index.isFile() ){
                System.err.println( "Expecting '" + index.getAbsolutePath() + "' to be an audit-event index-file!" );
                System.exit( -1 );
            }
            
            
            File data = new File( params.removeFirst() );
            if( ! data.isDirectory() ){
                System.err.println( "Expecting '" + data.getAbsolutePath() + "' to be an audit-event data-directory!" );
                System.exit( -1 );
            }

            log.info( "Reading from index '" + index + "', data-directory is '" + data + "'..." );
            reader = new ConcurrentAuditReader( data, index );
        } else
            reader = new ModSecurity2AuditReader( new File( params.removeFirst() ) );
        

        AuditEvent evt = reader.readNext();

        long start = System.currentTimeMillis();
        DecimalFormat fmt = new DecimalFormat( "0.00" );
        int cnt = 0;

        while( evt != null && cnt < skip ){
            cnt++;
            evt = reader.readNext();
        }

        log.info( "Skipped " + cnt + " events." );

        while( evt != null && cnt < limit ){
            cnt++;

            if( randDelay > 0 ){

                try {
                    Double sleep = Math.random() * randDelay;
                    log.fine("Sleeping for " + sleep.intValue() + " ms" );
                    Thread.sleep( sleep.intValue() );
                } catch ( Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                sender.sendAuditEvent( evt );
            } catch (Exception e) {
                log.warning( "Failed to send event #" + cnt + ": " + e.getMessage() );
            }

            try {
                evt = reader.readNext();
            } catch (ParseException re ){
                log.warning("Failed read event, skipping this one!" );
            } catch ( Exception e) {
                log.warning("Failed read event, skipping this one!" );
            }

            if( cnt % 100 == 0 ){
                double time = (double) (System.currentTimeMillis() - start);
                double rate = 100.0d / (time / 1000.0d); 
                log.info( cnt + " events send (rate: " + fmt.format( rate ) + " evts/s  [Currently using " + memoryUsed() + " MB of memory]" );
                start = System.currentTimeMillis();
            }
        }

        reader.close();
    }
    
    
    public long memoryUsed(){
        return ( Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory() ) / ( 1024 * 1024 );
    }

    
    /**
     * @param args
     */
    public static void main(String[] args)
    throws Exception
    {
        EventSender sender = new EventSender();
        sender.run( args );
    }
}
