/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.util.List;
import java.util.logging.LogManager;

import javax.swing.JFileChooser;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.jwall.apache.httpd.config.ApacheConfig;
import org.jwall.apache.httpd.config.ConfigParser;

public class ApacheConfig2Html implements CommandLineTool {
	public final static String NAME = "apache2html";

	public static void createHtml(String xmlConfig, File out) throws Exception {

		URL url = ApacheConfig2Html.class.getResource("/config2html.xsl");
		if (url == null)
			throw new Exception("Cannot find XSLT style!");

		Source styleSource = new StreamSource(url.openStream());
		Source xmlSource = new StreamSource(
				(Reader) new StringReader(xmlConfig));

		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();
		Transformer transformer = transformerFactory
				.newTransformer(styleSource);

		System.out.println("Writing html to " + out.getAbsolutePath() + ".");
		StreamResult result = new StreamResult(new FileOutputStream(out));
		transformer.transform(xmlSource, result);
	}

	/**
	 * @param args
	 */
	public void run(String[] args) {

		try {

			LogManager lm = LogManager.getLogManager();

			URL url = ApacheConfig2Html.class
					.getResource("/logging.properties");
			if (url != null)
				lm.readConfiguration(url.openStream());

			ApacheConfig config = null;
			File out = new File("ApacheConfig.html");

			if (args.length == 0) {
				String usage = "\n"
						+ "  apache2html\n"
						+ "  -----------\n"
						+ "\n"
						+ "  This is the ApacheConfig2Html converter of jwall.org. It allows you to create an HTML\n"
						+ "  view of your apache configuration files.\n\n"
						+ "  Usage:\n"
						+ "         java  -jar jwall-tools.jar  apache2html  /path/to/httpd.conf [outfile.html] \n"
						+ "";

				System.out.println(usage);
				System.exit(0);
			}

			File httpdConf = new File(args[0]);
			System.out.println("Parsing config from "
					+ httpdConf.getAbsolutePath());

			ConfigParser parser = new ConfigParser(httpdConf);
			config = parser.parseConfig();

			if (args.length > 1)
				out = new File(args[1]);

			if (config != null) {

				String cfg = config.toXML();

				if (System.getProperty("keep-xml") != null) {

					File xmlFile = new File("apache-config.xml");
					FileWriter xmlOut = new FileWriter(xmlFile);
					xmlOut.write(cfg);
					xmlOut.flush();
					xmlOut.close();

				}

				createHtml(cfg, out);

			} else
				throw new Exception("Cannot parse ApacheConfig!");

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.exit(0);
	}

	public void start(List<String> args) throws Exception {
		ApacheConfig config = null;
		File httpdConf = null;
		File out = new File("ApacheConfig.html");

		if (args.isEmpty()) {

			JFileChooser jfc = new JFileChooser();
			int ret = jfc.showOpenDialog(null);
			if (ret == JFileChooser.APPROVE_OPTION) {
				httpdConf = jfc.getSelectedFile();
			}

		} else
			httpdConf = new File(args.get(0));

		if (httpdConf == null || !httpdConf.canRead())
			return;

		ConfigParser parser = new ConfigParser(httpdConf);
		config = parser.parseConfig();

		if (config != null) {

			String cfg = config.toXML();

			if (System.getProperty("keep-xml") != null) {

				File xmlFile = new File("apache-config.xml");
				FileWriter xmlOut = new FileWriter(xmlFile);
				xmlOut.write(cfg);
				xmlOut.flush();
				xmlOut.close();

			}

			createHtml(cfg, out);

			//
			// TODO: Here we should call the default browser to display the
			// page!
			//

		} else
			throw new Exception("Cannot parse ApacheConfig!");
	}

	public String getName() {
		return NAME;
	}

	public static void main(String[] args) throws Exception {
		ApacheConfig2Html cfg = new ApacheConfig2Html();

		System.setProperty("keep-xml", "true");
		String[] params = new String[] { "/Users/chris/test/etc/httpd/conf/httpd.conf" };
		if (args.length > 0)
			params = args;
		cfg.run(params);
	}
}
