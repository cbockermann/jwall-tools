/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.File;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.ImageIcon;

import org.apache.modsecurity.MSCollectionStore;
import org.jwall.apache.httpd.config.ApacheConfig;
import org.jwall.apache.httpd.config.Directive;
import org.solinger.sdbm.Sdbm;

public class CollectionViewer implements CommandLineTool {
	static Logger log = Logger.getLogger("CollectionViewer");

	public final static String PROPERTY_ALWAYS_RELOAD = "always-reload";

	static String usage = "\n" + "  ModSecurity Collection Viewer\n"
			+ "  -----------------------------\n\n" + "Usage:\n"
			+ "       java -jar " + Tools.TOOL_JAR
			+ "  collections  [OPTIONS] /path/to/SecDataDir\n" + "\n";

	static ImageIcon icon = null;
	public final static String NAME = "collections";

	/**
	 * @param args
	 */
	public void run(String[] args) {
		try {

			if (args.length == 0) {
				System.out.println(usage);
				System.exit(0);
			}

			boolean all = false;
			boolean verbose = false;
			boolean sortKeys = false;
			int refresh = -1;

			for (int i = 0; i < args.length - 1; i++) {

				if (args[i].equalsIgnoreCase("--all")
						|| args[i].equalsIgnoreCase("-a"))
					all = true;

				if (args[i].equalsIgnoreCase("--refresh")
						|| args[i].equalsIgnoreCase("-r")) {
					if (i + 1 < args.length && args[i + 1].matches("\\d+")) {

						refresh = Integer.parseInt(args[i + 1]);

					} else {
						throw new Exception(
								"Option \"--refresh\" needs an integer as parameter!\n");
					}
				}

				if (args[i].equals("--large-sdbm") || args[i].equals("-l")) {
					System.setProperty(Sdbm.LARGE_DB_PROPERTY, "true");
				}

				if (args[i].equals("--sort-keys") || args[i].equals("-s")) {
					sortKeys = true;
				}

				if (args[i].equals("--strict-reload"))
					System.setProperty(PROPERTY_ALWAYS_RELOAD, "true");

				if (args[i].equals("--verbose") || args[i].equals("-v"))
					verbose = true;

				if (args[i].equalsIgnoreCase("--help") || args[i].equals("-h")) {
					System.out.println(usage);
					System.exit(0);
				}
			}

			File config = new File(args[args.length - 1]);
			if (!config.canRead()) {

				throw new Exception("Cannot read file "
						+ config.getAbsolutePath());

			}

			File dataDir = null;
			if (config.isFile()) {

				ApacheConfig cfg = ApacheConfig.parse(config);

				List<Directive> directives = cfg.findDirectiveByName(
						"SecDataDir", true);
				if (directives.isEmpty()) {

					throw new Exception(
							"No \"SecDataDir\" directive found in your config!");

				} else {

					Directive secDataDir = directives.get(0);
					dataDir = new File(secDataDir.getArgs().get(0));
				}
			} else
				dataDir = config;

			System.out.println("Reading collections from "
					+ dataDir.getAbsolutePath());

			MSCollectionStore store = new MSCollectionStore(dataDir);
			System.out.println(store.toString(all, verbose, sortKeys));

			while (refresh > 0) {
				Thread.sleep(refresh * 1000);
				System.out
						.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
				cls();
				store.reload();
				System.out.print(store.toString(all, verbose, sortKeys));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void cls() throws Exception {
		Runtime.getRuntime().exec("clear");
	}

	public String getName() {
		return "CollectionViewer";
	}

	public ImageIcon getIcon() {
		return icon;
	}

	public String getDescription() {
		return "Allows for the inspection of ModSecurity collection files.";
	}

	public void start(List<String> args) throws Exception {
		if (args.isEmpty()) {
			File config = new File(args.get(0));
			File dataDir = null;
			if (config.isFile()) {

				ApacheConfig cfg = ApacheConfig.parse(config);

				List<Directive> directives = cfg.findDirectiveByName(
						"SecDataDir", true);
				if (directives.isEmpty()) {

					throw new Exception(
							"No \"SecDataDir\" directive found in your config!");

				} else {

					Directive secDataDir = directives.get(0);
					dataDir = new File(secDataDir.getArgs().get(0));
				}
			} else
				dataDir = config;

			System.out.println("Reading collections from "
					+ dataDir.getAbsolutePath());

			MSCollectionStore store = new MSCollectionStore(dataDir);
			System.out.println(store.toString(true, true, true));
		}
	}

	public static void main(String args[]) {
		CollectionViewer v = new CollectionViewer();
		v.run(args);
	}
}
