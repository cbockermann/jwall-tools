/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.File;
import java.net.URI;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;

import org.jwall.web.sensor.TCPIndexListener;
import org.jwall.web.sensor.UDPIndexListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.runtime.StreamRuntime;

public class Mlogcd implements CommandLineTool {

	static Logger log = LoggerFactory.getLogger(Mlogcd.class);
	final LinkedBlockingQueue<Data> queue = new LinkedBlockingQueue<Data>();

	@Override
	public String getName() {
		return "mlogcd";
	}

	@Override
	public void run(String[] args) throws Exception {
		StreamRuntime.setupLogging();

		List<String> params = Tools.handleArguments(args);
		if (params.size() < 2) {
			System.err
					.print("At least 2 parameters are required:   the mlogc.conf file and a listener URL!");
		}

		Properties properties = Mlogc
				.readMlogcConfig(new File(params.remove(0)));
		for (Object key : properties.keySet()) {
			log.info("   '{}' = '{}'", key.toString(),
					properties.getProperty(key.toString()));
		}

		URI receiver = new URI(params.remove(0));
		log.info("Receiver URI is '{}'", receiver);

		Thread consumer = new Thread() {
			public void run() {
				while (true) {
					try {
						Data item = queue.take();
						log.info("Processing item: {}", item);
					} catch (Exception e) {
						try {
							Thread.sleep(500);
						} catch (Exception ex) {
						}
					}
				}
			}
		};
		consumer.start();

		if ("udp".equalsIgnoreCase(receiver.getScheme())) {
			String host = receiver.getHost();
			Integer port = receiver.getPort();

			UDPIndexListener listener = new UDPIndexListener(queue, host, port);
			log.info("Created new index-listener: {}", listener);
			listener.start();

			log.info("Waiting for listener to finish...");
			listener.join();
		}

		if ("tcp".equalsIgnoreCase(receiver.getScheme())) {
			String host = receiver.getHost();
			Integer port = receiver.getPort();

			TCPIndexListener listener = new TCPIndexListener(queue, host, port);
			log.info("Created new index-listener: {}", listener);
			listener.start();

			log.info("Waiting for listener to finish...");
			listener.join();
		}

	}

	public static void main(String[] args) throws Exception {

		String[] as = new String[] { "/opt/modsecurity/etc/mlogc.conf",
				"tcp://127.0.0.1:12345" };

		Mlogcd mlogc = new Mlogcd();
		mlogc.run(as);
	}

}