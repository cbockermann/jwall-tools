/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.jwall.audit.processor.DNSLookup;
import org.jwall.util.MacroExpander;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.filter.FilterCompiler;
import org.jwall.web.audit.filter.FilterExpression;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;
import org.jwall.web.audit.util.Cache;

public class ReverseDNS implements CommandLineTool {

	PrintStream err = System.err;
	boolean verbose = false;
	String format = "%{REMOTE_ADDR}\t%{REMOTE_HOSTNAME}";
	Set<String> addresses = new HashSet<String>();
	Cache<String> fwdCache = new Cache<String>(10000);

	@Override
	public String getName() {
		return "rdns";
	}

	public void printHelp() {
		URL url = ReverseDNS.class.getResource("/help/rdns.txt");
		System.out.println();
		Tools.print(url, "    ");
		System.out.println();
	}

	public String forwardLookup(String addr) {

		if (this.fwdCache.containsKey(addr))
			return fwdCache.get(addr);

		try {
			InetAddress inet = InetAddress.getByName(addr);
			return inet.getHostAddress();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void run(String[] args) throws Exception {

		long limit = Long.MAX_VALUE;
		PrintStream out = System.out;
		String progress = "";
		boolean showProgress = "true".equalsIgnoreCase(System
				.getProperty("progress"));
		boolean unique = "true".equalsIgnoreCase(System.getProperty("unique"));
		verbose = "true".equalsIgnoreCase(System.getProperty("verbose"));

		if (System.getProperty("format") != null) {
			format = System.getProperty("format");
		}

		if ("true".equalsIgnoreCase(System.getProperty("help")) || args == null
				|| args.length == 0) {
			this.printHelp();
			return;
		}

		if (System.getProperty("errors") == null)
			System.setProperty("errors", "rdns-error.log");

		try {
			File ef = new File(System.getProperty("errors"));
			System.err.println("Writing log to " + ef.getAbsolutePath());
			err = new PrintStream(new FileOutputStream(ef));
		} catch (Exception e) {
			err = System.err;
		}

		if (System.getProperty("output") != null) {
			File of = new File(System.getProperty("output"));
			out = new PrintStream(new FileOutputStream(of));
			System.err.println("Writing output to " + of.getAbsolutePath());
		}

		FilterExpression validation = null;
		if (System.getProperty("validation") != null) {
			try {
				validation = FilterCompiler.parse(System
						.getProperty("validation"));
			} catch (Exception e) {
				err.println("Failed to compile validation filter: "
						+ e.getMessage());
				System.exit(-1);
			}
		}

		String filterString = System.getProperty("filter");
		FilterExpression filter = null;

		try {
			limit = new Long(System.getProperty("limit"));
		} catch (Exception e) {
			limit = Long.MAX_VALUE;
		}

		if (filterString != null) {
			try {
				System.err.println("Using filter '" + filterString + "'");
				filter = FilterCompiler.parse(filterString);
			} catch (Exception e) {
				err.println("Invalid filter: '" + filterString + "', error: "
						+ e.getMessage());
				System.exit(-1);
			}
		}

		if (args.length == 0) {
			System.err.println("No input-file given!");
			System.exit(-1);
		}

		for (String arg : args) {
			File f = new File(arg);
			if (f.canRead())
				System.err.println("Processing file: " + f.getAbsolutePath());
		}

		for (String arg : args) {
			File input = new File(arg);
			if (input.canRead()) {

				Long totalBytes = input.length();

				int fmt = AuditFormat.guessFormat(input);
				if (fmt < 0) {
					System.err
							.println("Failed to determine input file format!");
					System.exit(-1);
				}

				System.err.println("File format is "
						+ AuditFormat.FORMAT_NAMES[fmt]);

				AuditEventReader reader = AuditFormat.createReader(
						input.getAbsolutePath(), false);
				// DNSLookupProcessor dns = new DNSLookupProcessor();
				DNSLookup dns = new DNSLookup();
				dns.setCacheSize(10000);
				dns.setTarget("REMOTE_HOST");

				System.err.println("Scanning file '" + input.getAbsolutePath()
						+ "'");
				final Map<String, Object> context = new HashMap<String, Object>();
				final MacroExpander macro = new MacroExpander();
				AuditEvent evt = reader.readNext();
				while (evt != null && limit > 0) {

					// resolve REMOTE_ADDR => REMOTE_HOST
					//
					dns.processEvent(evt, context);

					if (filter == null || filter.matches(evt)) {

						String addr = evt.get(dns.getKey());

						verbose("Checking address '" + addr + "'");
						String name = evt.get(dns.getTarget());

						verbose("   resolved to '" + name + "'");

						try {

							String inetAddr = dns.lookup(name);

							verbose("   forward lookup returned: '" + inetAddr
									+ "'");

							evt.set("ADDRESS:REMOTE_HOST", inetAddr);

							// check reverse mapping: addr(REMOTE_HOST) ==
							// REMOTE_ADDR
							//
							if (!addr.equals(inetAddr)) {
								//
								// if this mapping/reverse-mapping fails, we
								// assume something is wrong...
								//
								logError("Hostname '" + name
										+ "' does not map back to address '"
										+ addr + "' but to '" + inetAddr
										+ "' instead!");

							} else {
								//
								// an optional validation check
								//
								if (validation != null
										&& !validation.matches(evt)) {
									//
									// the user-specified validation failed!
									//
									logError("Validation failed for request from address: "
											+ addr
											+ " ("
											+ name
											+ "), with user-agent: "
											+ evt.get("REQUEST_HEADERS:User-Agent"));

								} else {
									//
									// everything was validated successfully,
									// now we print the address
									// if we did not print it before or 'unique'
									// is set to false
									//
									if (!unique || !addresses.contains(addr)) {
										String expanded = macro.expand(format,
												evt);
										out.println(expanded);
									}

									if (unique)
										addresses.add(addr);
								}
							}
						} catch (Exception e) {
							logError("Error while looking up '" + addr + "': "
									+ e.getMessage());
						}
					}

					Long read = reader.bytesRead();
					if (showProgress && limit % 1000 == 0) {
						DecimalFormat df = new DecimalFormat("0");
						String st = df.format(100 * read.doubleValue()
								/ totalBytes.doubleValue())
								+ "%";
						if (!st.equalsIgnoreCase(progress)) {
							System.err.println(st + " read");
							progress = st;
						}
					}

					evt = reader.readNext();
					limit--;
				}

				reader.close();
			}
		}
	}

	public void verbose(String msg) {
		if (verbose)
			System.err.println(msg);
	}

	public void logError(String msg) {
		err.print(new Date());
		err.print(": ");
		err.println(msg);
	}

	public void debug(String msg) {
		System.err.println(msg);
	}
}