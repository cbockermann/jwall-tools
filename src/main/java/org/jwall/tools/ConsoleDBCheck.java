/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jwall.util.JarFileLoader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ConsoleDBCheck implements CommandLineTool {

	File baseDir;
	String dbUrl = "jdbc:mysql://localhost:3306/AuditConsoleDB";
	String dbDriver = "com.mysql.jdbc.Driver";
	String dbUser = "console";
	String dbPassword = "console";
	
	long auditEventCount = 0L;
	long eventMessageCount = 0L;
	long tagCount = 0L;
	long fileCount = 0L;

	@Override
	public String getName() {
		return "console-db-check";
	}



	protected void loadLibraryJars() throws Exception {
		int libs = 0;
		File libDir = new File( baseDir.getAbsolutePath() + File.separator + "lib" + File.separator + "console/WEB-INF/lib/" );
		for( File file : libDir.listFiles() ){
			if( file.isFile() && file.getName().endsWith( ".jar" ) ){
				//System.out.println( "   Adding JAR " + file.getName() );
				JarFileLoader.addPath( file.getAbsolutePath() );
				libs++;
			}
		}
		System.out.println( "\t * " + libs + " jars added from AuditConsole installation." );
	}


	protected void checkDatabaseConfig() throws Exception {
		System.out.println( "Determining database config..." );
		Class.forName( "com.mysql.jdbc.Driver" );

		File config = new File( baseDir.getAbsolutePath() + File.separator + "conf" + File.separator + "AuditConsole.xml" );
		if( config.isFile() ){
			System.out.println( "Loading configuration from '" + config.getAbsolutePath() + "'" );

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse( config );

			NodeList list = doc.getElementsByTagName( "DatabaseConfig" );
			if( list.getLength() < 1 ){
				throw new Exception( "No database-config found in '" + config.getAbsolutePath() + "'!" );
			}

			Element node = (Element) list.item( 0 );
			NodeList children = node.getChildNodes();
			for( int i = 0; i < children.getLength(); i++ ){

				Node n = children.item(i);
				//System.out.println( "Checking node '" + n.getNodeName() + "'" );
				if( n.getNodeType() == Node.ELEMENT_NODE ){

					Element ch = (Element) n;
					if( ch.getNodeName().equalsIgnoreCase( "url" ) ){
						dbUrl = ch.getTextContent();
						System.out.println( "   database-url is: '" + dbUrl + "'");
					}

					if( ch.getNodeName().equalsIgnoreCase( "User" ) ){
						dbUser = ch.getTextContent();
						System.out.println( "   database-user is: '" + dbUser + "'");
					}

					if( ch.getNodeName().equalsIgnoreCase( "Password" ) ){
						dbPassword = ch.getTextContent();
						System.out.println( "   database-password is: '" + dbPassword + "'");
					}
				}
			}
			
			if( dbUrl == null )
				throw new Exception( "No database URL found in configuration!" );
			
			if( dbUrl.toLowerCase().startsWith( "jdbc:mysql" ) ){
				dbDriver = "com.mysql.jdbc.Driver";
			}
			
			if( dbUrl.toLowerCase().startsWith( "jdbc:postgres" ) ){
				dbDriver = "org.postgres.jdbc.Driver";
			}
			
			if( dbUrl.toLowerCase().startsWith( "jdbc:derby" ) ){
				dbDriver = "org.apache.derby.EmbeddedDriver";
			}
			
			System.out.println( "   Using database driver '" + dbDriver + "'" );

		} else {
			throw new Exception( "Did not find AuditConsole.xml configuration file!" );
		}
	}


	protected void checkDatabaseStats() throws Exception {
		Connection c = null;
		try {
			c = DriverManager.getConnection( dbUrl, dbUser, dbPassword );

			Statement stmt = c.createStatement();
			System.out.println( "\tTrying to determine the number of indexed events:" );
			ResultSet rs = stmt.executeQuery( "SELECT COUNT(*) FROM AUDIT_EVENTS" );
			if( rs.next() ){
				auditEventCount = rs.getLong( 1 );
				System.out.println( "\t  * " + auditEventCount + " events stored in index table AUDIT_EVENTS." );
			}
			rs.close();

			// count related event messages
			//
			rs = stmt.executeQuery( "SELECT COUNT(MSG_ID) FROM EVENT_MESSAGES" );
			if( rs.next() ){
				eventMessageCount = rs.getLong( 1 );
				System.out.println( "\t  * " + eventMessageCount + " event-messages in table EVENT_MESSAGES." );
			}
			rs.close();

			rs = stmt.executeQuery( "SELECT COUNT(TAG_ID) FROM TAGS" );
			if( rs.next() ){
				tagCount = rs.getLong( 1 );
				System.out.println( "\t  * " + tagCount + " event-tags in table TAGS." );
			}
			rs.close();

			rs = stmt.executeQuery( "SELECT COUNT(*) FROM AUDIT_EVENT_FILES" );
			if( rs.next() ){
				fileCount = rs.getLong( 1 );
				System.out.println( "\t  * " + fileCount + " raw data chunks (files) stored in table AUDIT_EVENT_FILES." );
			}
			rs.close();
			
			checkLostEvents( c );

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if( c != null )
					c.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	
	private void checkLostEvents( Connection c) throws Exception {
		System.out.println( "\n\tChecking for lost records..." );
		try {
			Statement stmt = c.createStatement();
			
			ResultSet rs = stmt.executeQuery( "SELECT COUNT(MSG_ID) FROM EVENT_MESSAGES WHERE ID NOT IN ( SELECT ID FROM AUDIT_EVENTS )" );
			if( rs.next() ){
				System.out.println( "\t  * " + rs.getInt( 1 ) + " event messages with no event associated (event deleted?)." );
			}
			rs.close();
			
			rs = stmt.executeQuery( "SELECT COUNT(TX_ID) FROM AUDIT_EVENTS WHERE TX_ID NOT IN ( SELECT TX_ID FROM AUDIT_EVENT_FILES )" );
			if( rs.next() ){
				System.out.println( "\t  * " + rs.getInt( 1 ) + " events without a data file." );
			}
			rs.close();
			
			rs = stmt.executeQuery( "SELECT COUNT(TX_ID) FROM AUDIT_EVENT_FILES WHERE TX_ID NOT IN ( SELECT TX_ID FROM AUDIT_EVENTS )" );
			if( rs.next() ){
				System.out.println( "\t  * " + rs.getInt( 1 ) + " raw data chunks (events) stored in data table, but not indexed (possibly half-deleted)." );
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public void run(String[] args) throws Exception {

		if( args.length < 1 ){
			System.err.println( "Missing argument!" );
			System.err.println( "You need to specify the AuditConsole directory!" );
			System.exit(-1);
		}

		baseDir = new File( args[0] );
		System.out.println( "  jwall-tools: console-db-check" );
		System.out.println( "  -----------------------------\n" );
		System.out.println( "  Checking AuditConsole in directory " + baseDir.getAbsolutePath() );
		System.out.println( "" );

		// Load the libs for this AuditConsole (including DB drivers!)
		//
		System.out.println( "  (1) Loading libraries" );
		loadLibraryJars();
		System.out.println();

		// determine Database configuration
		//
		System.out.println( "  (2) Checking database config..." );
		checkDatabaseConfig();
		System.out.println();

		// gather information about the database contents...
		//
		System.out.println( "  (3) Gathering database statistics..." );
		checkDatabaseStats();
		System.out.println();
	}


	public static void main( String[] args ) throws Exception {
		ConsoleDBCheck check = new ConsoleDBCheck();
		check.run( new String[]{ "/opt/AuditConsole" } );
	}
}