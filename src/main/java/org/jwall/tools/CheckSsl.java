/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.security.cert.CRLReason;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLEntry;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.jwall.ssl.CrlCollector;
import org.jwall.ssl.CrlLocation;
import org.jwall.ssl.HttpsChecker;

import stream.util.URLUtilities;

/**
 * @author chris
 * 
 */
public class CheckSsl implements CommandLineTool {

	/**
	 * @see org.jwall.tools.CommandLineTool#getName()
	 */
	@Override
	public String getName() {
		return "check-ssl";
	}

	/**
	 * @see org.jwall.tools.CommandLineTool#run(java.lang.String[])
	 */
	@Override
	public void run(String[] args) throws Exception {

		final CrlCollector col = new CrlCollector();

		HttpsChecker https = new HttpsChecker();
		https.addHandler(col);

		String host = args[0];
		int port = 443;
		if (args.length > 1) {
			port = Integer.parseInt(args[1]);
		}

		https.check(host, port);

		boolean verbose = "true"
				.equalsIgnoreCase(System.getProperty("verbose"));

		System.out.println("Certificate chain referenced "
				+ col.collectedUrls().size() + " CRL URLs.");
		CertificateFactory cf = CertificateFactory.getInstance("X509");

		SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");

		for (CrlLocation url : col.collectedUrls()) {
			try {
				File file = new File("/opt/jwall/var/data/crls/" + url.id()
						+ "/" + fmt.format(new Date()) + ".crl");

				file.getParentFile().mkdirs();

				URL curl = new URL(url.url());

				URLUtilities.copy(curl, file);

				X509CRL crl = (X509CRL) cf
						.generateCRL(new FileInputStream(file));

				Date update = crl.getThisUpdate();
				Date nextUpdate = crl.getNextUpdate();

				System.out.println("Checking CRL at '" + url.url() + "'");
				// log.info("Update time of CRL: {}", update);
				// log.info("Next update of CRL: {}", nextUpdate);

				Set<? extends X509CRLEntry> revoked = crl
						.getRevokedCertificates();

				if (revoked == null) {
					revoked = new HashSet<X509CRLEntry>();
				}

				System.out.println("+------BEGIN-CRL------------");
				System.out.println("| URL:         '" + url.url() + "'");
				System.out
						.println("| Issuer:      '" + crl.getIssuerDN() + "'");
				System.out.println("|");
				System.out.println("| Certificates revoked:   "
						+ revoked.size());
				System.out.println("| Last update:            "
						+ crl.getThisUpdate());
				System.out.println("| Next update:            "
						+ crl.getNextUpdate());
				System.out.println("|");
				// log.info("CRL contains {} revoked certificates.",
				// revoked.size());

				if (verbose) {
					for (X509CRLEntry entry : revoked) {
						System.out.println("|");
						String hex = "0x"
								+ entry.getSerialNumber().toString(16);

						System.out.println("|   Certificate['" + hex
								+ "'].issuer:     "
								+ entry.getCertificateIssuer());
						System.out
								.println("|   Certificate['" + hex
										+ "'].revokedAt:  "
										+ entry.getRevocationDate());

						CRLReason reason = entry.getRevocationReason();
						String rs = "?unknown?";
						if (reason != null) {
							rs = reason.name();
						}
						System.out.println("|   Certificate['" + hex
								+ "'].revokeReason:  " + rs);

						// log.info("entry is of class {}", entry.getClass());
						// log.info("   revoked[{}].serial:    {}", i, entry
						// .getSerialNumber().toString(16));
						// log.info("   revoked[{}].issuer:    {}", i,
						// entry.getCertificateIssuer());
						// log.info("   revoked[{}].revokedAt: {}", i,
						// revokedAt);
					}
					System.out.println("|");
				}

				System.out.println("+------END-CRL---------------");

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}