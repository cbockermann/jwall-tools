/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.jwall.web.audit.AuditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * This class performs a simple macro-expansion based on a pattern string and a
 * ModSecurity audit-log event.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class MacroExpander {

	/* A global logger for this class */
	static Logger log = LoggerFactory.getLogger(MacroExpander.class);

	public final static String VAR_PREFIX = "%{";
	public final static String VAR_SUFFIX = "}";

	/* The variables available in this context */
	Map<String, String> variables = new HashMap<String, String>();

	public MacroExpander() {
		this(new HashMap<String, String>());
	}

	public MacroExpander(Map<String, String> variables) {
		this.variables = variables;
	}

	public MacroExpander(Properties p) {
		this.variables = new HashMap<String, String>();
		for (Object k : p.keySet())
			variables.put(k.toString(), p.getProperty(k.toString()));
	}

	public void addVariables(Map<String, String> vars) {
		for (String key : vars.keySet())
			variables.put(key, vars.get(key));
	}

	public void set(String key, String val) {
		variables.put(key, val);
	}

	public String expand(String str, AuditEvent evt) {
		return substitute(str, evt);
	}

	private String substitute(String str, AuditEvent evt) {
		String content = str;
		int start = content.indexOf(VAR_PREFIX, 0);
		while (start >= 0) {
			int end = content.indexOf(VAR_SUFFIX, start);
			if (end >= start + 2) {
				String variable = content.substring(start + 2, end);
				log.debug("Found variable: {}", variable);
				log.trace("   content is: {}", content);
				String val = get(variable, evt);
				if (val == null) {
					val = "";
				}

				content = content.replace(VAR_PREFIX + variable + VAR_SUFFIX,
						val);
				start = content.indexOf(VAR_PREFIX, start + 2);
			} else
				start = -1;
		}
		return content;
	}

	public String get(String variable, AuditEvent evt) {
		if (evt.isSet(variable)) {
			log.debug("Found variable '{}' in event: '{}'", variable,
					evt.get(variable));
			return evt.get(variable);
		}

		return variables.get(variable);
	}

}