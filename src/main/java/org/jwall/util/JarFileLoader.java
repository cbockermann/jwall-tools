/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.util;

import java.io.File;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class JarFileLoader 
extends URLClassLoader 
{
	
	public JarFileLoader(){
		super( new URL[0] );
	}

	public JarFileLoader (URL[] urls)
	{
		super (urls);
	}

	public void addFile (String path) throws MalformedURLException
	{
		String urlPath = "jar:file://" + path + "!/";
		addURL (new URL (urlPath));
	}


	public static void addPath(String s) throws Exception {
		File f = new File(s);
		URL u = f.toURI().toURL();
		URLClassLoader urlClassLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
		Class<?> urlClass = URLClassLoader.class;
		Method method = urlClass.getDeclaredMethod("addURL", new Class[]{URL.class});
		method.setAccessible(true);
		method.invoke(urlClassLoader, new Object[]{u});
	}
	
	
	
	public static void main (String args [])
	{
		try
		{
			System.out.println ("First attempt...");
			Class.forName ("org.gjt.mm.mysql.Driver");
		}
		catch (Exception ex)
		{
			System.out.println ("Failed.");
		}

		try
		{
			URL urls [] = {};

			JarFileLoader cl = new JarFileLoader (urls);
			cl.addFile ("/opt/mysql-connector-java-5.0.4/mysql-connector-java-5.0.4-bin.jar");
			System.out.println ("Second attempt...");
			cl.loadClass ("org.gjt.mm.mysql.Driver");
			System.out.println ("Success!");
		}
		catch (Exception ex)
		{
			System.out.println ("Failed.");
			ex.printStackTrace ();
		}
	}
}
