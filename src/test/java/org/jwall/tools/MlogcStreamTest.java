/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import org.junit.Test;
import org.jwall.log.io.SequentialFileInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chris
 * 
 */
public class MlogcStreamTest {

	static Logger log = LoggerFactory.getLogger(MlogcStreamTest.class);

	@Test
	public void test() {
		try {

			int files = 10;
			int linesPerFile = 100;

			FileSequenceGenerator g = new FileSequenceGenerator();
			g.generate(new File("/tmp"), files, linesPerFile);

			int total = files * linesPerFile;

			SequentialFileInputStream sfis = new SequentialFileInputStream(
					new File("/tmp/index"), "index*", false);

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					sfis));
			String line = reader.readLine();
			File cur = sfis.getCurrentFile();
			int count = 0;
			if (line != null)
				count++;

			String last = null;

			int lines = 0;

			while (line != null) {
				lines++;
				line = reader.readLine();
				if (!cur.equals(sfis.getCurrentFile())) {
					log.info("File {} ended after {} lines.", cur, count);
					log.info("last line of {} was: {}", cur, last);
					cur = sfis.getCurrentFile();
					log.info("File changed to {}", cur);
					count = 1;
				} else {
					count++;
				}
				last = line;
			}

			log.info("Expected {} lines, read: {}", total, lines);

			reader.close();
			// URL url = MlogcStreamTest.class.getResource("/mlogc.xml");
			// stream.run.main(url);
		} catch (Exception e) {
			fail("Test failed: " + e.getMessage());
		}
	}

}
