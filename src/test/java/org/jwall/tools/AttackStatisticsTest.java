/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   Copyright (C) 2010-2014 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package org.jwall.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;

import org.jwall.util.MultiSet;
import org.jwall.web.audit.stats.io.CsvMultiSetReader;
import org.jwall.web.audit.stats.io.CsvMultiSetWriter;
import org.jwall.web.audit.stats.io.MultiSetReader;
import org.jwall.web.audit.stats.io.MultiSetUtils;
import org.jwall.web.audit.stats.io.MultiSetWriter;

public class AttackStatisticsTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
		throws Exception
	{
		String[] params = args;
		if( args.length == 0 ){
			params = new String[]{ "/www/pg542-audit.log" };
		}
		
		MessageStatistics st = new MessageStatistics();
		
		File f = null;
		if( params.length > 0 ){
			f = new File( params[0] );
			if( f.exists() ){
				st.process( new FileInputStream( f ) );
			}
		}
		
		URL url = AttackStatisticsTest.class.getResource( "/test-messages.dat" );
		if( f == null ){
			System.out.println( "Processing messages from " + url );
			st.process( url.openStream() );
		}
		
		MultiSetWriter w = new CsvMultiSetWriter( new FileOutputStream( "/tmp/rule-stats.csv" ) );
		w.write( st.getMsgStats() );
		w.close();
		
		MultiSetReader r = new CsvMultiSetReader( new FileInputStream( "/tmp/rule-stats.csv" ) );
		MultiSet<String> s = r.readNext();
		
		System.out.println( "Messages:" );
		System.out.println( MultiSetUtils.toJSON( s ) );
		
		PrintStream out = new PrintStream( new FileOutputStream( "/tmp/data.st" ) );
		out.println( MultiSetUtils.toJSON( s ) );
		out.close();
		
		url = new URL( "http://localhost:8080/stats/upload" );
		URLConnection con = url.openConnection();
		con.setDoOutput( true );
		con.setDoInput( false );
		
		out = new PrintStream( con.getOutputStream() );
		out.println( MultiSetUtils.toJSON( s ) );
		out.flush();
		out.close();
	}
}
