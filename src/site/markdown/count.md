Count
=====

The `count` command simply provides a convenient way to count
the number of events from a given ModSecurity audit log file
in *serial audit-log* format.


Usage
-----

Simply calling the `count` command with a serial audit-log
file will check the number of events in that file:

      # jwall count /path/to/audit.log