Introduction
============

The `jwall-tools` started as a small collection of Java based tools to
ease the management/handling of ModSecurity audit-log files. This collection
has been grown over time as several new tools have been added.

Currently it provides tools for creating HTML views of Apache configurations,
inspect/monitor ModSecurity collections or re-inject a set of HTTP requests
from audit-log data back into a web server.

Some of the tools are dedicated to very specific tasks, for example to upgrade
an [AuditConsole](http://www.jwall.org/web/audit/console/index.jsp) instance 
to the latest version, etc.


Getting Help
------------

The `jwall-tools` are provided as an open-source project by Christian Bockermann. For any problems regarding the configuration, setup or feature-requests, simply write an e-mail to `chris (at) jwall.org`.
