AuditLogConverter
=================

The `convert` command of the jwall-tools is intended to extract parts
of the audit-log entries and print them to the standard output or another
file.

Usage
-----

The `convert` command takes at least 2 parameters, which are a PATTERN
string and a file to read events from. Optionally one can also specify
an OUTPUT file:

    jwall convert PATTERN AUDIT_LOG [OUTPUT_FILE]
    
The first parameter PATTERN is an arbitrary format string, which may
contain macros starting with the `%{` prefix and ending with `}`. Within
the macro, any ModSecurity variables can be used.

The AUDIT_LOG parameter denots the file to read audit-event data from.
This file is expected to be in ModSecurity 2.x serial audit-log format.

If you want to have the results written to a file instead of the standard
output, simple specify an OUTPUT_FILE as last argument.


Example
-------

An example to simple extract all request URIs from a file, you may call
the converter like:
    
    # jwall convert "uri=%{REQUEST_URI}" /path/to/audit.log [output.log]

