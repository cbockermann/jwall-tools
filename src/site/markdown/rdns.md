The jwall-tools' <code>rdns<code> Command
=========================================

The `rdns` command is designed to extract IP addresses from log files
(access-log and ModSecurity audit-log files). The extracted IP
addresses are resolved via reverse DNS lookups.

The overall goal is to verify the ownership of the IP address using
some external database (e.g. DNS, Whois, IP-lists). The verified
addresses will be written to standard output (or file) and can be used
to create white-list rules in ModSecurity.

Currently this ownership verification process is solely based on DNS
lookups. Please see the example section for details.

Options The jwall-tools' `rdns` command supports several options for
selecting the events to be checked and the exact output to be written:

  * `--filter="..."` allows you to specify a filter string of the events
    that should be checked, e.g.
      `REQUEST_HEADERS:User-Agent @rx google.*bot`

  * `--format="..."`, this option allows you to specify the exact output
   format for each line that has been validated. By default this
   format is set to `__%{REMOTE_ADDR}__`. Other format strings allow you
   to add more information to the output (see the example section).

Example
-------

The folowing example shows the method used to assess the origin of a
request's source IP address:

Suppose that a HTTP request is received from `1.2.3.4` with a User-Agent
claiming that it is a Google Bot. The address `1.2.3.4` maps to
`4-3-2-1.crawl.google.com`. This may be a good clue supporting that the
address is coming from a real google crawler.

With a second DNS request we resolve `4-3-2-1.crawl.google.com` and
expect it to resolve to the original address `1.2.3.4`. The reason for
this additional DNS query is that a malicious user could set up a
reverse DNS record to point an arbitrary IP address to
`4-3-2-1.crawl.google.com`.

The jwall-tools' `rdns` command can be used to run these checks on all
IPs extracted from an audit-log or access-log file:

     # jwall rdns --filter="REQUEST_HEADERS:UserAgent @rx google.*bot" \
                  --format="__%{REMOTE_ADDR}__Google" \
                  /path/to/access.log 

The output of this command will be something like:

      __1.2.3.4__Google
      __1.2.3.5__Google
      ...

That list can be used to check a Google Bot request against this list
using ModSecurity rules:

     # Create a variable for the IP address:
     #
     SecAction pass,setvar:tx.ip_g='__%{REMOTE_ADDR}__Google'

     # Perform IP check for GoogleBots:
     #
     SecRule REQUEST_HEADERS:User-Agent "google.*bot" \
            "deny,chain,msg:'Found unvalidated Google Bot request!'"
     SecRule TX:IP_G  "!@pmFromFile /path/to/google-ip.list"


Enhancements
------------

The approach outlined above solely focuses on the DNS records as a
source for IP validation. This is not the only possible approach. One
major drawback of this approach is that it validates a single IP
addresses at a time.

A more complex approach would be to use other databases like Whois or
similar to query whole network blocks to be added to the validation
list. That way, the validation of a single address might trigger the
validation of a complete subnet.

The Whois database will being integrated into future versions of the
`rdns` tool.
