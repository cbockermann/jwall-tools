ModSecurity Collections
=======================

The ModSecurity modules allows for maintaining persistent collections of
(*key*,*value*) tuples within the runtime. This can be used to provide a
context of values spanned over multiple requests.

With the `collections` command of the *jwall-tools* it is possible to
inspect the current state of the ModSecurity collections. The data for
the collections is stored in an DBM file by ModSecurity. The `collections`
tool reads that file (periodically, if requested) and outputs the current
values to the consol.

Usage
-----

The simplest use of the `collections` command is to dump the current
values to standard output. This can be done by calling the command with
the *data directory* of ModSecurity as argument, e.g.:

      # jwall collections /opt/modsecurity/var/data

This will make the `collections` command check for any `.pag` and `.dir`
files (from DBM) and dump their content to the console in the way they
are available within ModSecurity.


Options
-------

As it is sometimes convenient to keep an updated view of the state of
ModSecurity collections, the `-r` switch can be used to periodically
dump the collection contents. The following call will continuously dump
the collection values every 2 seconds:

      # jwall collections -r 2 /opt/modsecurity/var/data/

As ModSecurity also maintains meta information as "hidden" values in
the collections (e.g. timestamps), the output can be quite overwhelming.
Therefore, these meta information is supressed by default. With the
`-a` (or `--all`) switch, the `collections` command will dump out all
the keys and their values.

Finally, it may be convenient to have the keys sorted in alphabetical
order. With the `-s` (or `--sort-keys`) switch, the output is ordered
appropriately.